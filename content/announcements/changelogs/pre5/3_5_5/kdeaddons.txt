2006-08-09 10:30 +0000 [r571342]  mueller

	* trunk/extragear/utils/Makefile.am.in,
	  trunk/extragear/graphics/Makefile.am.in,
	  branches/KDE/3.5/kdeaddons/Makefile.am.in,
	  branches/KDE/3.5/kdebase/Makefile.am.in,
	  branches/KDE/3.5/kdeedu/Makefile.am.in,
	  branches/KDE/3.5/kdesdk/Makefile.am.in,
	  branches/koffice/1.5/koffice/Makefile.am.in,
	  branches/KDE/3.5/kdepim/Makefile.am.in,
	  branches/KDE/3.5/kdeaccessibility/Makefile.am.in,
	  branches/KDE/3.5/kdeadmin/Makefile.am.in,
	  branches/KDE/3.5/kdenetwork/Makefile.am.in,
	  branches/KDE/3.5/kdelibs/Makefile.am.in,
	  branches/KDE/3.5/kdeartwork/Makefile.am.in,
	  branches/arts/1.5/arts/Makefile.am.in,
	  trunk/extragear/pim/Makefile.am.in,
	  branches/KDE/3.5/kdemultimedia/Makefile.am.in,
	  branches/KDE/3.5/kdegames/Makefile.am.in,
	  trunk/playground/sysadmin/Makefile.am.in,
	  trunk/extragear/network/Makefile.am.in,
	  trunk/extragear/libs/Makefile.am.in,
	  branches/KDE/3.5/kdebindings/Makefile.am.in,
	  branches/KDE/3.5/kdetoys/Makefile.am.in,
	  trunk/extragear/multimedia/Makefile.am.in,
	  branches/KDE/3.5/kdeutils/Makefile.am.in,
	  branches/KDE/3.5/kdegraphics/Makefile.am.in: update automake
	  version

2006-08-12 12:08 +0000 [r572366]  apaku

	* branches/KDE/3.5/kdeaddons/kate/make/configure.in.in (added),
	  branches/KDE/3.5/kdeaddons/kate/make/plugin_katemake.cpp: BUG:
	  126052 Add support for finding the right make command during
	  configure time.

2006-08-17 10:09 +0000 [r573834]  mueller

	* branches/KDE/3.5/kdeaddons/kate/make/configure.in.in (removed),
	  branches/KDE/3.5/kdeaddons/kate/make/plugin_katemake.cpp: don't
	  hardcode paths

2006-08-20 16:34 +0000 [r575021]  osterfeld

	* branches/KDE/3.5/kdeaddons/konq-plugins/akregator/pluginbase.cpp:
	  fix completion of URLs starting with "/", e.g.
	  baseurl=http://foobar.com/feeds, link=/feeds/atom.xml BUG: 132687

2006-08-20 18:08 +0000 [r575065]  osterfeld

	* branches/KDE/3.5/kdeaddons/konq-plugins/akregator/pluginbase.cpp,
	  branches/KDE/3.5/kdeaddons/konq-plugins/akregator/akregatorplugin.cpp:
	  This should fix the crash when using the akregator plugin in a
	  file manager icon view (KonqKfmIconView). I can't reproduce the
	  bug, so I'm not 100% sure if it really fixes the problem. Please
	  test with >= 3.5.5 and reopen if the bug is still there. BUG:
	  124891

2006-08-20 19:39 +0000 [r575103]  stoecker

	* branches/KDE/3.5/kdeaddons/konq-plugins/babelfish/plugin_babelfish.cpp:
	  BUG: 131672 Fixed Japanese to English translation.

2006-08-21 15:05 +0000 [r575430]  jriddell

	* branches/KDE/3.5/kdeaddons/COPYING-DOCS (added): Add FDL licence
	  for documentation

2006-08-25 12:39 +0000 [r577054]  osterfeld

	* branches/KDE/3.5/kdeaddons/konq-plugins/akregator/konqfeedicon.cpp:
	  add and remove feed icon correctly when plugin is
	  enabled/disabled BUG: 132823

2006-08-27 10:23 +0000 [r577658]  deller

	* branches/KDE/3.5/kdeaddons/kaddressbook-plugins/xxports/gmx/gmx_xxport.cpp:
	  fix formatted names when importing

2006-09-23 15:55 +0000 [r587667]  kling

	* branches/KDE/3.5/kdeaddons/kicker-applets/kolourpicker/kolourpicker.cpp,
	  branches/KDE/3.5/kdeaddons/kicker-applets/kolourpicker/kolourpicker.h:
	  Remove placeholder "Help" dialog, since it's only confusing. BUG:
	  133264

2006-09-28 18:20 +0000 [r589831]  mueller

	* branches/KDE/3.5/kdeaddons/kicker-applets/ktimemon/sample.cc: fix
	  kernel memory being counted twice. Thanks for the patch!
	  BUG:134704

2006-10-02 11:07 +0000 [r591323]  coolo

	* branches/KDE/3.5/kdeaddons/kdeaddons.lsm: updates for 3.5.5

