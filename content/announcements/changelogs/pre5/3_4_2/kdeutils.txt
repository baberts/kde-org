2005-05-24 20:39 +0000 [r417863]  dgp

	* configure.in.in, ksim/monitors/filesystem/configure.in.in: Avoid
	  (most) warnings during ./configure on FreeBSD.

2005-05-27 01:56 +0000 [r418596]  henrique

	* ark/arkwidget.h, ark/arkwidget.cpp: * Backport the fix for bug
	  #105398

2005-05-27 02:02 +0000 [r418598]  henrique

	* ark/main.cpp: * Bump version number

2005-06-03 20:33 +0000 [r421742]  goutte

	* khexedit/Makefile.am: I18N: untranslatable strings - there are
	  i18n() calls in subdirectories - there is a rc file in one
	  subdirectory - however there is no i18nop() calls (Backport of
	  revision 421738) CCMAIL:kde-i18n-doc@kde.org

2005-06-04 14:12 +0000 [r422100]  jlee

	* kjots/kjotsedit.h, kjots/KJotsSettings.kcfgc,
	  kjots/kjotsentry.cpp, kjots/KJotsMain.cpp, kjots/kjotsedit.cpp,
	  kjots/kjotsentry.h, kjots/kjots.kcfg, kjots/KJotsMain.h:
	  Backported much work that fixes several bugs and problems. Some
	  of these bugs can cause data loss, crashes, and unintended
	  behaviour. See the bug tracker and commit logs for more
	  information. Rev #416292 Rev #416396 Rev #417154 Rev #417158
	  BUG:106059 Rev #417221 BUG:78403 Rev #417223 Rev #418225
	  BUG:94290 Rev #418247 Rev #418249 Rev #418274 Rev #419116
	  BUG:97156

2005-06-04 14:24 +0000 [r422102]  jlee

	* kjots/main.cpp: Version bump and maintainer change.

2005-06-07 14:35 +0000 [r423100]  mueller

	* kmilo/powerbook2/pb_monitor.cpp: fix compile failure with
	  pbbuttons >= 0.6.9 BUG: 104517

2005-06-08 15:17 +0000 [r423486]  thiago

	* kcalc/kcalc_core.cpp: Backporting commit 423478 by goutte to the
	  branch. This fixes the 0+0 and 0-0 glitch (0+0 != NaN).
	  BUG:106605

2005-06-18 18:40 +0000 [r426873]  jlee

	* kjots/kjotsentry.cpp: Bug: 105925 Carriage returns inserted as
	  spacers will now print correctly.

2005-06-27 15:18 +0000 [r429401]  binner

	* kdeutils.lsm: 3.4.2 preparations

2005-07-05 10:37 +0000 [r431823]  hasso

	* ark/arkwidget.cpp: Backport of i18n("Open") usage audit. No new
	  strings (i18n("to open", "Open") is already used in
	  kmail/kmcomposewin.cpp).

2005-07-15 17:59 +0000 [r434873]  lukas

	* kwallet/konfigurator/konfigurator.cpp: missing i18n()

