------------------------------------------------------------------------
r854583 | bram | 2008-08-29 22:47:56 +0200 (Fri, 29 Aug 2008) | 1 line

Backporting r853842: Adapt to current comic url (patch by Matthias Fuchs)
------------------------------------------------------------------------
r857261 | scripty | 2008-09-05 08:09:42 +0200 (Fri, 05 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r857793 | annma | 2008-09-06 14:47:38 +0200 (Sat, 06 Sep 2008) | 2 lines

backport r857301: suppress unneeded resize which affects performance

------------------------------------------------------------------------
r862596 | mfuchs | 2008-09-19 13:33:06 +0200 (Fri, 19 Sep 2008) | 4 lines

Backport r 852811 r 852708
The comicprovider does not ignore the proxy settings anymore (Bug 168504
)

------------------------------------------------------------------------
r862982 | woebbe | 2008-09-20 17:32:52 +0200 (Sat, 20 Sep 2008) | 1 line

-pedantic
------------------------------------------------------------------------
r864256 | ilic | 2008-09-24 13:09:07 +0200 (Wed, 24 Sep 2008) | 1 line

Use KDE's date formatting, like in digital clock. (bport: 864255)
------------------------------------------------------------------------
