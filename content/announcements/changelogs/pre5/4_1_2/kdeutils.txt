------------------------------------------------------------------------
r854896 | lueck | 2008-08-30 17:08:12 +0200 (Sat, 30 Aug 2008) | 1 line

documentation backport fron trunk
------------------------------------------------------------------------
r855889 | pino | 2008-09-01 19:12:43 +0200 (Mon, 01 Sep 2008) | 4 lines

extract the messages from the .rc files too

CCMAIL: kde-i18n-doc@kde.org

------------------------------------------------------------------------
r856186 | pino | 2008-09-02 12:25:03 +0200 (Tue, 02 Sep 2008) | 4 lines

extract messages from .ui files too

CCMAIL: kde-i18n-doc@kde.org

------------------------------------------------------------------------
r856457 | dakon | 2008-09-02 23:35:52 +0200 (Tue, 02 Sep 2008) | 3 lines

loading photo ids now also works on Windows

backport of r856456
------------------------------------------------------------------------
r856596 | adiaferia | 2008-09-03 12:46:04 +0200 (Wed, 03 Sep 2008) | 1 line

since QDir was not included it didn't compile =)
------------------------------------------------------------------------
r856764 | lueck | 2008-09-03 20:22:39 +0200 (Wed, 03 Sep 2008) | 6 lines

revive blockdevices documentation, got lost somehow in kde 3.x
in trunk commit 856757
Could someone please copy stable/l10n/lang/docmessages/kdeutils/kinfocenter_blockdevices.po stable/l10n-kde4/de/docmessages/kdeutils/kcontrol_blockdevices.po
and in trunk
copy stable/l10n/lang/docmessages/kdeutils/kinfocenter_blockdevices.po trunk/l10n-kde4/de/docmessages/kdeutils/kcontrol_blockdevices.po
CCMAIL:kde-i18n-doc@kde.org
------------------------------------------------------------------------
r858923 | scripty | 2008-09-09 08:01:42 +0200 (Tue, 09 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r861428 | scripty | 2008-09-16 08:10:13 +0200 (Tue, 16 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r861760 | dakon | 2008-09-17 08:37:20 +0200 (Wed, 17 Sep 2008) | 4 lines

Do not reset filename after encrypt/decrypt operation in editor

BUG:168703

------------------------------------------------------------------------
r861761 | dakon | 2008-09-17 08:40:39 +0200 (Wed, 17 Sep 2008) | 2 lines

Fix crash if gpg process for key generation exits before "generating key" message is shown

------------------------------------------------------------------------
r861765 | scripty | 2008-09-17 08:52:59 +0200 (Wed, 17 Sep 2008) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r864331 | metellius | 2008-09-24 16:46:50 +0200 (Wed, 24 Sep 2008) | 2 lines

Backfix of bug 170614, originally fixed with revision 854362

------------------------------------------------------------------------
