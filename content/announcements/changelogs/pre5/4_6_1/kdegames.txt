------------------------------------------------------------------------
r1216225 | schwarzer | 2011-01-22 15:02:02 +1300 (Sat, 22 Jan 2011) | 2 lines

replace dialog with overlay message

------------------------------------------------------------------------
r1216229 | schwarzer | 2011-01-22 15:25:22 +1300 (Sat, 22 Jan 2011) | 3 lines

Revert "replace dialog with overlay message"

Sorry, this was supposed to go to trunk.
------------------------------------------------------------------------
r1216231 | schwarzer | 2011-01-22 15:31:19 +1300 (Sat, 22 Jan 2011) | 6 lines

prepare for release (version number)

unfortunately a bit late this time



------------------------------------------------------------------------
r1217037 | scripty | 2011-01-26 02:54:38 +1300 (Wed, 26 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1217463 | scripty | 2011-01-28 01:03:17 +1300 (Fri, 28 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1217758 | scripty | 2011-01-29 02:04:06 +1300 (Sat, 29 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1217876 | schwarzer | 2011-01-29 23:12:53 +1300 (Sat, 29 Jan 2011) | 1 line

Changelog
------------------------------------------------------------------------
r1217885 | scripty | 2011-01-30 00:48:17 +1300 (Sun, 30 Jan 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1218069 | wrohdewald | 2011-01-31 14:18:47 +1300 (Mon, 31 Jan 2011) | 1 line

Backport: add missing modeltest.py to CMakeLists.txt
------------------------------------------------------------------------
r1218281 | coates | 2011-02-02 09:29:01 +1300 (Wed, 02 Feb 2011) | 7 lines

Backport of commit 1218280.

Fix improper labelling of Klondike game types.

BUG:264871
FIXED-IN:SC4.6.1

------------------------------------------------------------------------
r1218283 | coates | 2011-02-02 09:41:13 +1300 (Wed, 02 Feb 2011) | 6 lines

Backport of commit 1218282.

Fix issues with loading Draw 3 Klondike games.

Man, I hate switch statements.

------------------------------------------------------------------------
r1219630 | coates | 2011-02-10 08:47:06 +1300 (Thu, 10 Feb 2011) | 2 lines

Bump the version number to 3.5.1 for KDE SC 4.6.1.

------------------------------------------------------------------------
r1219988 | wrohdewald | 2011-02-12 15:40:08 +1300 (Sat, 12 Feb 2011) | 8 lines

Backport from trunk: Fix config dialog

The definition of the background plain_color changed in Aug 2010
but I never noticed... because kajongg was using 4.5 version
from /usr/share
So the config dialog broke in 4.6. Fixed now.


------------------------------------------------------------------------
r1220725 | wrohdewald | 2011-02-15 11:42:14 +1300 (Tue, 15 Feb 2011) | 6 lines

Backport from trunk: aborting a game did not correctly log off the server
    
so when logging in next time the aborted game was shown as
running instead of suspended


------------------------------------------------------------------------
r1220735 | wrohdewald | 2011-02-15 12:03:21 +1300 (Tue, 15 Feb 2011) | 5 lines

fix unicode bug
    
scoring game: In the list of suspended games display of utf-8 did
not work 

------------------------------------------------------------------------
r1221502 | scripty | 2011-02-19 02:49:22 +1300 (Sat, 19 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1222394 | scripty | 2011-02-24 02:10:57 +1300 (Thu, 24 Feb 2011) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
