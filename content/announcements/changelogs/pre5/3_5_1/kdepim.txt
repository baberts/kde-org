2005-11-28 10:08 +0000 [r483822]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/akregator_view.cpp: don't
	  open two external browser windows when opening the feed homepage

2005-11-28 10:43 +0000 [r483829]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/actionmanagerimpl.cpp:
	  backport: enable open homepage action correctly, but only if
	  htmlurl is non-empty

2005-11-29 14:03 +0000 [r484134]  dfaure

	* branches/KDE/3.5/kdepim/libkdepim/addresseelineedit.cpp: Handle
	  <tab> like <arrow down> in completion list, to skip section
	  headers BUG: 117217

2005-11-29 23:04 +0000 [r484247]  gianni

	* branches/KDE/3.5/kdepim/kmail/kmmainwidget.cpp: so the bug
	  #116054 it's been backported to 3.5 branch too CCBUG:116054

2005-12-01 12:02 +0000 [r484668]  danders

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttView.cpp,
	  branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.cpp,
	  branches/KDE/3.5/kdepim/kdgantt/KDGanttView.h: Add signal
	  gvLinkContextMenuRequested to allow for context menus also for
	  task links.

2005-12-01 13:18 +0000 [r484688]  danders

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttView.cpp,
	  branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.cpp,
	  branches/KDE/3.5/kdepim/kdgantt/KDGanttView.h: Undoing -r 484668,
	  shouldn't have been to branch.

2005-12-03 12:48 +0000 [r485221]  mlaurent

	* branches/KDE/3.5/kdepim/kmail/kmmainwin.rc,
	  branches/KDE/3.5/kdepim/kmail/kmmainwidget.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmail_part.rc: Don't add separator
	  when we don't add filter into "apply filter menu"

2005-12-04 18:08 +0000 [r485516-485514]  gungl

	* branches/KDE/3.5/kdepim/kmail/kmkernel.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmcommands.cpp: fix the crash when
	  using pipe-through filters in combination with the
	  actionscheduler BUG:113730

	* branches/KDE/3.5/kdepim/kmail/kmheaders.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmcommands.cpp: improve usability
	  during long-lasting filter operations

2005-12-04 21:02 +0000 [r485543]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/Changelog,
	  branches/KDE/3.5/kdepim/kalarm/kalarm.h,
	  branches/KDE/3.5/kdepim/kalarm/mainwindow.cpp,
	  branches/KDE/3.5/kdepim/kalarm/mainwindow.h: Fix toolbar
	  configuration being lost after quitting KAlarm

2005-12-05 09:23 +0000 [r485647]  mlaurent

	* branches/KDE/3.5/kdepim/kmail/kmmainwin.rc,
	  branches/KDE/3.5/kdepim/kmail/kmail_part.rc: This entry doesn't
	  exist

2005-12-05 15:43 +0000 [r485739]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/feed.cpp: If an fetch error
	  occurs (host down, parsing error), wait 30 minutes before trying
	  again. Akregator retried to fetch the feed every minute, which is
	  particularily painful if Akregator fails to parse an actually
	  valid feed. BUG: 113358

2005-12-06 00:21 +0000 [r485866]  winterz

	* branches/KDE/3.5/kdepim/korganizer/resourceview.cpp: Still saving
	  the calendar resources config file immediately, but in a more
	  sane manner. I was making this more difficult than necessary.
	  Thanks for the feedback Reinhold.

2005-12-06 19:16 +0000 [r486093]  dfaure

	* branches/KDE/3.5/kdepim/certmanager/lib/tests/test_cryptoconfig.cpp:
	  Convert source file to utf8

2005-12-06 20:40 +0000 [r486116]  staikos

	* branches/KDE/3.5/kdepim/korganizer/komonthview.h,
	  branches/KDE/3.5/kdepim/korganizer/komonthview.cpp: make
	  korganizer usable again. was reading the entire addressbook O(n)
	  for n being the number of events. big addressbook + big calendar
	  == very long time to do any operation. It's still too slow, but
	  now it takes less than 30 seconds to load and less than 10
	  seconds to flip between months.

2005-12-06 20:49 +0000 [r486122]  dfaure

	* branches/KDE/3.5/kdepim/korganizer/koprefs.cpp: This is where the
	  real bug is (compared to earlier versions, at least). It makes no
	  sense for a sync call to trigger an async loading of the
	  addressbook. Especially when this means we'll trigger it again
	  and again and again while it hasn't finished. CCMAIL:
	  tokoe@kde.org, kde-pim@kde.org

2005-12-06 20:54 +0000 [r486123]  dfaure

	* branches/KDE/3.5/kdepim/korganizer/koprefs.cpp: Same fix there:
	  back to sync loading (if needed) for sync call.

2005-12-06 21:03 +0000 [r486127]  mueller

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttMinimizeSplitter.cpp: very
	  funny

2005-12-06 21:27 +0000 [r486141-486140]  gungl

	* branches/KDE/3.5/kdepim/kmail/actionscheduler.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmfilteraction.cpp: Fix some issues
	  in the async filtering code BUG:113730

	* branches/KDE/3.5/kdepim/kmail/kmfolder.cpp: Better handling of
	  folder types when creating a folder dir

2005-12-07 17:40 +0000 [r486379]  rytilahti

	* branches/KDE/3.5/kdepim/akregator/src/akregator_part.rc: backport
	  r486363. hope this is okay.

2005-12-07 19:54 +0000 [r486422]  gungl

	* branches/KDE/3.5/kdepim/kmail/actionscheduler.cpp,
	  branches/KDE/3.5/kdepim/kmail/actionscheduler.h,
	  branches/KDE/3.5/kdepim/kmail/kmfilteraction.cpp: completion of
	  the fix for bug 113730

2005-12-07 23:21 +0000 [r486486]  winterz

	* branches/KDE/3.5/kdepim/korganizer/koprefs.h,
	  branches/KDE/3.5/kdepim/korganizer/korganizer.kcfg,
	  branches/KDE/3.5/kdepim/korganizer/koprefs.cpp: Move the default
	  TimeBar font from the .kcfg file to the koprefs code. Else, we
	  can get crashes in QFont constructor when passing a null font
	  name.

2005-12-08 14:04 +0000 [r486703]  dfaure

	* branches/KDE/3.5/kdepim/kmail/callback.cpp,
	  branches/KDE/3.5/kdepim/libkpimidentities/identitymanager.cpp,
	  branches/KDE/3.5/kdepim/libkpimidentities/identity.cpp,
	  branches/KDE/3.5/kdepim/libkpimidentities/identity.h: Don't use a
	  global object in a library, the order of initialization is
	  undefined so it can crash if e.g. QString::null hasn't been
	  initialized yet. Using a pointer instead, with delayed
	  initialization.

2005-12-08 23:25 +0000 [r486839]  kloecker

	* branches/KDE/3.5/kdepim/kmail/kmversion.h: New version number for
	  KMail with fixed IMAP+Client side filtering+Spamassassin == mail
	  loss bug.

2005-12-10 04:08 +0000 [r487310]  gungl

	* branches/KDE/3.5/kdepim/kmail/kmfoldertree.cpp: Disable folders
	  in IMAP and cached IMAP accounts as destination for a folder move
	  to avoid loosing all messages in the subfolders when moving
	  nested folders. Moving to a local destination folder works
	  correct. This is only a workaround for the real problem which is
	  addressed by a patch. It was aggreed on not applying the patch to
	  the KDE 3.5 branch before it's been tested by more people.
	  BUG:114985

2005-12-10 04:41 +0000 [r487311]  gungl

	* branches/KDE/3.5/kdepim/kmail/renamejob.cpp,
	  branches/KDE/3.5/kdepim/kmail/renamejob.h: Fix moving of nested
	  folders to a non-local destination folder. Messages in the
	  subfolders don't get lost anymore. This functionality is hidden
	  as it was decided to disable the moving of nested folders to IMAP
	  and DIMAP accounts. The change refers to bug report 114985.

2005-12-10 17:22 +0000 [r487450]  tokoe

	* branches/KDE/3.5/kdepim/kaddressbook/imagewidget.cpp,
	  branches/KDE/3.5/kdepim/kaddressbook/imagewidget.h: Fixed several
	  bugs in image input handling I know the patch is rather large for
	  a branch, but the problems could only be solved by structural
	  changes. BUGS:104526,117153,117758

2005-12-10 17:45 +0000 [r487459]  tokoe

	* branches/KDE/3.5/kdepim/kaddressbook/xxport/vcard_xxport.cpp:
	  Bugfix of #116006 BUGS:116006

2005-12-12 10:59 +0000 [r487871]  dfaure

	* branches/KDE/3.5/kdepim/kmail/acljobs.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmfolderimap.cpp: Fix performance
	  problem especially visible with .kde being nfs-mounted: during
	  IMAP sync, every message would be marked as 'dirty' due to
	  setStatus() calls, even when the status didn't change, and at the
	  end of the folder this resulted in the index entry for every
	  message being rewritten. + improved a kdWarning in acljobs.

2005-12-13 15:08 +0000 [r488193-488192]  burghard

	* branches/KDE/3.5/kdepim/kmail/kmheaders.cpp: Remove backtrace.

	* branches/KDE/3.5/kdepim/kmail/imapaccountbase.cpp: Empty strings
	  are read from the config as empty strings and not QString::null
	  BUGS: 115254

2005-12-13 15:47 +0000 [r488203]  burghard

	* branches/KDE/3.5/kdepim/kmail/newfolderdialog.cpp: Set the
	  imapPath also when no namespace combo was available

2005-12-13 16:16 +0000 [r488215]  burghard

	* branches/KDE/3.5/kdepim/kmail/subscriptiondialog.cpp: Fix UI bug
	  as reported by Casey Allen Shobe: when you click OK on the dialog
	  that tells you that subscriptions are not enabled but then do not
	  save the subscription dialog nothing is changed. Therefore show
	  the dialog only when something actually changed and subscriptions
	  are needed. CCMAIL: cshobe@seattleserver.com

2005-12-14 07:46 +0000 [r488380]  djarvie

	* branches/KDE/3.5/kdepim/libkcal/icalformatimpl.cpp: Bug 118286:
	  fix alarm attachments not working

2005-12-14 08:16 +0000 [r488384]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/Changelog: Bug 118286: fix alarm
	  attachments not working

2005-12-14 22:01 +0000 [r488547]  savernik

	* branches/KDE/3.5/kdepim/kaddressbook/kabcore.cpp: Fix overzealous
	  caching of iterators. Don't cache this collection's end iterator
	  while iterating over another one. BUG 117749

2005-12-15 01:06 +0000 [r488589]  winterz

	* branches/KDE/3.5/kdepim/kresources/birthdays/resourcekabcconfig.cpp:
	  Fix the bug in the birthdays resource configuration dialog that
	  caused KOrganizer/Kontact to crash.

2005-12-15 21:03 +0000 [r488768]  winterz

	* branches/KDE/3.5/kdepim/kontact/plugins/specialdates/sdsummarywidget.cpp,
	  branches/KDE/3.5/kdepim/kontact/plugins/specialdates/sdsummarywidget.h:
	  Fix for holidays going whacky. Apparently, the libkholidays stuff
	  isn't re-entrant. In any event, one day (far, far away) we will
	  rewrite libkholidays and it will be wonderful. So watch for KDE
	  3.5.1 to get this fix. BUGS: 118381, 117545

2005-12-15 21:09 +0000 [r488773-488770]  winterz

	* branches/KDE/3.5/kdepim/kontact/plugins/specialdates/sdsummarywidget.cpp:
	  whoopsie. Put back the configuration value names consistent with
	  the 3.5.x releases. I had mistakenly committed the new
	  configuration value names I plan to use for the 4.0 release.

	* branches/KDE/3.5/kdepim/kontact/plugins/specialdates/sdsummarywidget.cpp:
	  ..and this one too.

2005-12-15 22:20 +0000 [r488792]  toma

	* branches/KDE/3.5/kdepim/kmail/kmmainwidget.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmail.kcfg: Make the time before
	  showing the splash when opening a mailfolder on imap configurable
	  in the ini. BUG: 116773

2005-12-16 01:20 +0000 [r488825]  winterz

	* branches/KDE/3.5/kdepim/konsolekalendar/konsolekalendar.cpp: fix
	  for problem of exporting all events to HTML doesn't actually
	  print all the events.

2005-12-16 10:38 +0000 [r488898]  hasso

	* branches/KDE/3.5/kdepim/libkcal/attendee.cpp,
	  branches/KDE/3.5/kdepim/libkcal/incidence.cpp: Needs different
	  translation in Estonian, possibly in other languages as well.
	  CCMAIL: kde-i18n-doc@kde.org CCMAIL: kde-et@linux.ee

2005-12-16 16:18 +0000 [r488973]  tilladam

	* branches/KDE/3.5/kdepim/kmail/objecttreeparser.cpp: Properly
	  parse encoded message attachments by making sure the decoded body
	  part is used to construct a new message from. proko2 issue 963

2005-12-16 18:40 +0000 [r488996]  tilladam

	* branches/KDE/3.5/kdepim/kmail/kmreaderwin.cpp: Allow opening of
	  message attachments which are quoted printable encoded in a
	  properly decoded manner.

2005-12-17 17:37 +0000 [r489209]  luis_pedro

	* branches/KDE/3.5/kdepim/indexlib/main.cpp: Add remove_doc option
	  No output if search returns an empty list More debugging options

2005-12-19 19:39 +0000 [r489792-489791]  mueller

	* branches/KDE/3.5/kdepim/kmail/kmfoldermaildir.cpp: return when
	  can't read the cwd

	* branches/KDE/3.5/kdepim/kmail/kmfoldermaildir.cpp: *sigh*

2005-12-19 19:50 +0000 [r489797]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/mainwindow.cpp: Fix undefined
	  return value

2005-12-19 19:51 +0000 [r489798]  winterz

	* branches/KDE/3.5/kdepim/korganizer/exportwebdialog.cpp,
	  branches/KDE/3.5/kdepim/libkcal/htmlexport.h,
	  branches/KDE/3.5/kdepim/libkcal/htmlexportsettings.kcfg,
	  branches/KDE/3.5/kdepim/libkcal/htmlexport.cpp: Provide the
	  ability to print the incidence's Location field to the html
	  export. In konsolekalendar, the incidence location will always be
	  printed to the html. In korganizer, the location field print is
	  configurable. Note that permission has been granted by the docs
	  team to introduce a new i18n string with this patch. Note also
	  that this patch partially fixes several other bugs, like #56713
	  and #63626. CCMAIL: reinhold@kainhofer.com BUGS: 117676

2005-12-19 20:13 +0000 [r489800]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/messagewin.cpp,
	  branches/KDE/3.5/kdepim/kalarm/alarmevent.h,
	  branches/KDE/3.5/kdepim/kalarm/Changelog,
	  branches/KDE/3.5/kdepim/kalarm/messagewin.h,
	  branches/KDE/3.5/kdepim/kalarm/kalarm.h,
	  branches/KDE/3.5/kdepim/kalarm/alarmevent.cpp: Make autoclose of
	  message windows work

2005-12-19 20:49 +0000 [r489820]  adridg

	* branches/KDE/3.5/kdepim/kioslaves/mbox/readmbox.cc: Need
	  sys/types for utime on some platforms. CCMAIL: kde@freebsd.org

2005-12-20 19:31 +0000 [r490090]  gungl

	* branches/KDE/3.5/kdepim/kmail/avscripts/kmail_clamav.sh: Make the
	  script aware of running ClamAV daemons to speed up the detection.
	  Patch provided by Oscar Santacreu. Thanks, Oscar.
	  CCMAIL:oscar.santacreu@ua.es

2005-12-21 16:47 +0000 [r490368]  tilladam

	* branches/KDE/3.5/kdepim/kmail/kmail_part.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmail_part.h: Make sure that the
	  kmail config is reloaded when the kcms signal that things
	  changed, even from the part, which means from inside Kontact.

2005-12-22 08:08 +0000 [r490511]  tilladam

	* branches/KDE/3.5/kdepim/kmail/kmcomposewin.cpp: Forward port of:
	  SVN commit 490385 by tilladam: Force the configure entry in the
	  Settings meny in composer windows to read "Configure KMail" since
	  it brings up the kmail config dialog, not the Kontact one, even
	  in Kontact, which makes sense, but leads to a confusing lable.
	  Proko2 issue 738 ( c), then 1) )

2005-12-22 12:12 +0000 [r490571]  tilladam

	* branches/KDE/3.5/kdepim/kmail/kmail.kcfg,
	  branches/KDE/3.5/kdepim/kmail/kmsender.cpp: Forward port of: SVN
	  commit 490568 by tilladam: Implement a non-gui-exposed option to
	  allow sending of MDN messages with an SMTP MAIL FROM of <>, which
	  conforms to the rfc, but doesn't work with many real world
	  servers, since they discard such messages for spam protection.
	  Still, integrators might want to set it, via Kiosk, for example.

2005-12-23 15:45 +0000 [r490895]  vkrause

	* branches/KDE/3.5/kdepim/knode/articlewidget.cpp: Backport SVN
	  commit 490894 by vkrause for KDE 3.5.1: Fix bug #118521 (empty
	  article viewer ignores background color setting). BUG: 118521

2005-12-23 21:58 +0000 [r490962]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/recurrenceedit.cpp: Use
	  calendar-independent day and month name translations

2005-12-26 02:27 +0000 [r491411]  mhunter

	* branches/KDE/3.5/kdepim/kmail/kmail.kcfg: Typographical
	  corrections and changes

2005-12-27 12:43 +0000 [r491743]  mkelder

	* branches/KDE/3.5/kdepim/kioslaves/mbox/readmbox.cc,
	  branches/KDE/3.5/kdepim/kioslaves/mbox/readmbox.h: Backport.
	  Remove atEnd() calls and use the result of readLine to find out
	  if the file is at EOF. This should speed up reading files with a
	  large number of lines. BUG: 118926

2005-12-28 11:04 +0000 [r491991]  vkrause

	* branches/KDE/3.5/kdepim/knode/articlewidget.cpp: Backport commit
	  491989 for KDE 3.5.1: Fix crash on startup if auto mark-as-read
	  is disabled. BUG: 116514

2005-12-28 14:05 +0000 [r492025]  johach

	* branches/KDE/3.5/kdepim/knode/main.cpp,
	  branches/KDE/3.5/kdepim/knode/knode_part.cpp: Fix for
	  untranslated scoring rule dialog. Approved by Volker Krause.

2005-12-28 22:22 +0000 [r492152]  mkelder

	* branches/KDE/3.5/kdepim/korn/pop3_proto.cpp,
	  branches/KDE/3.5/kdepim/korn/imap_proto.cpp: I replaced the
	  i18n-string "Secure Socket Layer" through "SSL".

2005-12-29 09:30 +0000 [r492217]  johach

	* branches/KDE/3.5/kdepim/knode/main.cpp,
	  branches/KDE/3.5/kdepim/knode/knode_part.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmstartup.cpp: Removed loading of
	  obsolete catalogue "libkdenetwork" from knode and kmail. As
	  suggested by Ingo Kloecker and Nicolas Goutte. Added missing
	  catalogue "libkpgp" to knode.

2005-12-29 10:34 +0000 [r492231]  kloecker

	* branches/KDE/3.5/kdepim/kmail/configuredialog.cpp: Backport of
	  SVN commit 492128 by kloecker: Partial revert of 374245. Not all
	  occurrences of 'headers' refer to the message list. CCMAIL:
	  kde-i18n-doc@kde.org

2005-12-30 00:15 +0000 [r492457]  mhunter

	* branches/KDE/3.5/kdepim/kmail/antispamwizard.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmailicalifaceimpl.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmmainwidget.cpp,
	  branches/KDE/3.5/kdepim/kmail/configuredialog.cpp,
	  branches/KDE/3.5/kdepim/korganizer/korganizer.kcfg,
	  branches/KDE/3.5/kdepim/kmail/kmfoldercachedimap.cpp,
	  branches/KDE/3.5/kdepim/korganizer/calendarview.cpp,
	  branches/KDE/3.5/kdepim/kmailcvt/filter_lnotes.cxx,
	  branches/KDE/3.5/kdepim/korganizer/actionmanager.cpp,
	  branches/KDE/3.5/kdepim/korganizer/koagenda.cpp,
	  branches/KDE/3.5/kdepim/korganizer/datenavigatorcontainer.cpp,
	  branches/KDE/3.5/kdepim/korganizer/journalentry.cpp,
	  branches/KDE/3.5/kdepim/kresources/tvanytime/kcal_resourcetvanytime.cpp:
	  Typographical corrections and changes CCMAIL:kde-i18n-doc@kde.org

2005-12-30 08:36 +0000 [r492514]  tokoe

	* branches/KDE/3.5/kdepim/kaddressbook/addresseditwidget.cpp,
	  branches/KDE/3.5/kdepim/kaddressbook/addresseditwidget.h: Allow
	  the selection/copy of the formatted address by replacing QLabel
	  with KActiveLabel. BUGS: 118634

2005-12-30 13:16 +0000 [r492609]  burghard

	* branches/KDE/3.5/kdepim/kmail/kmmessage.cpp: Mimelib creates
	  empty header fields when you ask for them and they do not exist.
	  So retrieve the Content-Type field only when it is present.
	  Prevents display problems with Thunderbird as reported on
	  kdepim-users. CCMAIL:listen@alexander.skwar.name

2005-12-30 13:30 +0000 [r492613]  brade

	* branches/KDE/3.5/kdepim/knotes/ChangeLog,
	  branches/KDE/3.5/kdepim/knotes/knote.cpp: more correct enabling
	  of edit actions when (un)locking a note, found while porting to
	  Qt 4.

2006-01-02 14:23 +0000 [r493452]  scripty

	* branches/KDE/3.5/kdepim/knotes/icons/cr16-action-knotes_delete.png,
	  branches/KDE/3.5/kdepim/kmail/hi64-app-kmail.png,
	  branches/KDE/3.5/kdepim/kmail/hi128-app-kmail.png: Remove
	  svn:executable from some typical non-executable files (goutte)

2006-01-03 20:57 +0000 [r493980]  winterz

	* branches/KDE/3.5/kdepim/libkholidays/holidays/holiday_be: Move
	  Driekoningen to January 6. Unfortunately, by the time this is
	  patched in KDE 3.5.1 this holiday will have passed. BUGS:119456

2006-01-03 22:26 +0000 [r494011]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/messagewin.cpp,
	  branches/KDE/3.5/kdepim/kalarm/Changelog: Display alarm message
	  windows within current screen in multi-head systems.

2006-01-04 08:39 +0000 [r494184-494181]  mueller

	* branches/KDE/3.5/kdepim/akregator/src/pageviewer.cpp,
	  branches/KDE/3.5/kdepim/akregator/src/mk4storage/metakit/src/column.cpp:
	  avoid compiler warnings

	* branches/KDE/3.5/kdepim/kresources/groupwise/soap/incidenceconverter.cpp:
	  initialize your member variables

	* branches/KDE/3.5/kdepim/kresources/kolab/kcal/journal.cpp: avoid
	  compiler warning

	* branches/KDE/3.5/kdepim/karm/karmstorage.cpp: avoid compiler
	  warning

2006-01-04 21:46 +0000 [r494379]  brade

	* branches/KDE/3.5/kdepim/knotes/ChangeLog,
	  branches/KDE/3.5/kdepim/knotes/knote.cpp: Two more things I found
	  when porting to KDE 4: * never show the text of the tool buttons
	  * don't accept color drops on a locked note

2006-01-05 12:38 +0000 [r494502]  johach

	* branches/KDE/3.5/kdepim/korganizer/korgac/korgacmain.cpp: This
	  fixes an translation issue in the reminder popup window. Some
	  strings showed up untranslated. CCMAIL:kde-i18n-doc@kde.org

2006-01-06 12:12 +0000 [r494852]  mkoller

	* branches/KDE/3.5/kdepim/mimelib/datetime.cpp,
	  branches/KDE/3.5/kdepim/mimelib/dw_date.cpp: BUG:117848 Improve
	  parsing of non-standard date strings. It now parses in addition
	  date strings in the format "WWW MMM dd HH:MM:SS [Z] YYYY" zone is
	  optional e.g.: Fri Oct 14 09:21:49 CEST 2005 or: Tue Mar 23
	  18:00:02 2004

2006-01-06 15:09 +0000 [r494903]  lunakl

	* branches/KDE/3.5/kdepim/knotes/knote.cpp: Use
	  _NET_WM_MOVERESIZECANCEL (#101468).

2006-01-06 21:55 +0000 [r495039]  kloecker

	* branches/KDE/3.5/kdepim/kmail/kmcomposewin.cpp: Backport of
	  495038 (excluding the message fix): If the sender selected Send
	  Later then there's no point in showing this message box.

2006-01-06 22:25 +0000 [r495043]  kloecker

	* branches/KDE/3.5/kdepim/knotes/knote.cpp: Make KDE PIM 3.5
	  compile with KDE 3.4.

2006-01-06 22:38 +0000 [r495045]  brade

	* branches/KDE/3.5/kdepim/knotes/ChangeLog,
	  branches/KDE/3.5/kdepim/knotes/knoteedit.cpp: Cool, I keep
	  finding bugs that are easy to fix :-) Fix enabling/disabling of
	  font and font size combos according to the rich text setting.

2006-01-06 22:54 +0000 [r495049]  brade

	* branches/KDE/3.5/kdepim/kontact/plugins/knotes/knotes_part.cpp,
	  branches/KDE/3.5/kdepim/kontact/plugins/knotes/knotes_part_p.h:
	  Fix #117437. Linebreaks were not shown properly because non-rich
	  text notes were shown in a rich text interpreter. I wonder why it
	  worked before, the implementation was completely missing... BUG:
	  117437

2006-01-06 23:41 +0000 [r495062]  kloecker

	* branches/KDE/3.5/kdepim/knotes/knote.cpp: NET::MoveResizeCancel
	  is new in KDE 3.5.1. Just for clarification: KDE PIM 3.5 MUST
	  compile with kdelibs 3.4.x (and of course also with kdelibs
	  3.5.0).

2006-01-07 10:48 +0000 [r495148]  brade

	* branches/KDE/3.5/kdepim/knotes/ChangeLog: No tabs. SVN_SILENT

2006-01-07 16:18 +0000 [r495279]  brade

	* branches/KDE/3.5/kdepim/knotes/ChangeLog,
	  branches/KDE/3.5/kdepim/knotes/knotesui.rc: Fixed #103780:
	  Separate "Clear" action visually from Cut/Copy/Paste since it's
	  not doing anything with the clipboard. BUG: 103780

2006-01-07 17:20 +0000 [r495310]  brade

	* branches/KDE/3.5/kdepim/knotes/ChangeLog,
	  branches/KDE/3.5/kdepim/knotes/knote.cpp: fixed #110672: added a
	  "Do not show again" box to the delete note dialog BUG: 110672

2006-01-07 17:45 +0000 [r495319]  brade

	* branches/KDE/3.5/kdepim/knotes/knote.cpp: just move some code

2006-01-07 21:16 +0000 [r495357]  brade

	* branches/KDE/3.5/kdepim/knotes/ChangeLog,
	  branches/KDE/3.5/kdepim/knotes/knote.cpp: Fixed #113223: actually
	  set the "Keep Above/Below" bit when creating a note on startup. I
	  wondered what the hell was going on for too long (I couldn't
	  disable the "Keep Above" flag anymore) until Tobias pointed me to
	  commit #465269.... Will also be fixed for KDE 3.5.1. BUG: 113223

2006-01-08 11:37 +0000 [r495473]  brade

	* branches/KDE/3.5/kdepim/kontact/plugins/knotes/knotes_part.cpp:
	  Sometimes I really wonder what I was smoking some years ago...
	  (did I actually? ;-) Move the code to the correct position (after
	  KNotesResourceManager::addNewNote) so that we get an item out of
	  the dict.

2006-01-08 12:08 +0000 [r495480]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/article.cpp: Fix comparison
	  operators of Article, such as < and <=. This fixes problems with
	  QMap and articles having identical pubdate (operator< just
	  compared pubdates, and QMap consideres articles as equal when
	  items are not comparable). Fixes 114997 navigation problems and
	  prevents creation of a new item when selecting an unread dupe
	  article. (a new item was created as map lookup failed). BUG:
	  114997

2006-01-08 12:11 +0000 [r495482]  brade

	* branches/KDE/3.5/kdepim/kontact/plugins/knotes/knotes_part.cpp:
	  Another one bites the dust, fixed #54293: sort the iconview. BUG:
	  54293

2006-01-08 16:20 +0000 [r495668]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/aboutdata.h: bump version
	  to 1.2.1

2006-01-09 14:28 +0000 [r496024]  vkrause

	* branches/KDE/3.5/kdepim/knode/resource.h: Increment version
	  number for KDE 3.5.1.

2006-01-09 18:02 +0000 [r496098]  tilladam

	* branches/KDE/3.5/kdepim/libkcal/freebusy.h,
	  branches/KDE/3.5/kdepim/libkcal/period.cpp,
	  branches/KDE/3.5/kdepim/libkcal/icalformatimpl.cpp,
	  branches/KDE/3.5/kdepim/libkcal/freebusy.cpp,
	  branches/KDE/3.5/kdepim/libkcal/period.h: Forward port of: SVN
	  commit 496088 by tilladam: Implement operator< on periods, use
	  qHeapSort for sorting them in FreeBusy, and don't sort for each
	  added item when parsing a freebusy ical file, but append the
	  unsorted list then sort once. Together this makes parsing
	  freebusy files O(n*log n) instead of O(n^3), which is, shall we
	  say, very noticeable.

2006-01-10 00:16 +0000 [r496193]  brade

	* branches/KDE/3.5/kdepim/knotes/ChangeLog,
	  branches/KDE/3.5/kdepim/knotes/knotesapp.h,
	  branches/KDE/3.5/kdepim/knotes/knotesnetsend.h,
	  branches/KDE/3.5/kdepim/knotes/knotesnetrecv.cpp,
	  branches/KDE/3.5/kdepim/knotes/knotesnetrecv.h,
	  branches/KDE/3.5/kdepim/knotes/knotesapp.cpp,
	  branches/KDE/3.5/kdepim/knotes/knotesnetsend.cpp: rewrote the
	  networking code to use KNetwork: * fixed #110915: KNetwork has
	  better error reporting than the previous code, now it says
	  "connection actively refused" instead of "No such file or
	  directory" * fixed #110838: this one was caused by an
	  uninitialize variable, so it wasn't reproducible BUG: 110838,
	  110915

2006-01-10 09:47 +0000 [r496291]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/feedlistview.cpp,
	  branches/KDE/3.5/kdepim/akregator/src/feedlistview.h: Properly
	  remove subitems recursively from the item dict when deleting the
	  parent items. This should fix several crashes like: "Move a
	  folder, delete a subitem, Crash". BUG: 118659

2006-01-10 12:01 +0000 [r496329]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/articlelistview.cpp,
	  branches/KDE/3.5/kdepim/akregator/ChangeLog: select the next
	  article in the article list when deleting the only selected
	  article (doesn't apply when deleting multiple articles) BUG:
	  119724

2006-01-10 20:34 +0000 [r496580]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/Changelog,
	  branches/KDE/3.5/kdepim/kalarm/editdlg.cpp: Bug 119735: fix New
	  From Template not creating alarm if template contents are not
	  changed

2006-01-10 23:42 +0000 [r496678]  brade

	* branches/KDE/3.5/kdepim/kontact/plugins/knotes/knotes_part.cpp:
	  Fixed #119889: when renaming a note within the note editor of
	  Kontact update the view as well (i.e., the listview item). BUG:
	  119889

2006-01-11 00:05 +0000 [r496686]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/prefdlg.cpp,
	  branches/KDE/3.5/kdepim/kalarm/Changelog,
	  branches/KDE/3.5/kdepim/kalarm/prefdlg.h,
	  branches/KDE/3.5/kdepim/kalarm/editdlg.cpp,
	  branches/KDE/3.5/kdepim/kalarm/editdlg.h: Bug 119346: Reduce size
	  of Preferences dialog to fit in 1024x768 screen

2006-01-11 12:28 +0000 [r496820]  mueller

	* branches/KDE/3.5/kdepim/kmail/kmheaders.cpp: fix default sorting
	  order to be date ascending again

2006-01-11 14:34 +0000 [r496941]  mkoller

	* branches/KDE/3.5/kdepim/kmail/kmmsgbase.cpp: On parsing with
	  decodeRFC2047String and no encoding information is found, use the
	  defined fallback instead of the users system locale

2006-01-11 18:32 +0000 [r497015]  winterz

	* branches/KDE/3.5/kdepim/kmail/kmcomposewin.cpp: Fix crash during
	  a "insert file" in the composer. Patch provided by Goffredo
	  Baroncelli and approved by Don. Thank you Goffredo! BUGS: 111383,
	  108063, 111713 CCMAIL: kreijack@alice.it

2006-01-12 19:30 +0000 [r497436]  eschepers

	* branches/KDE/3.5/kdepim/kmail/partNode.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmedit.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmcomposewin.cpp: The QTimer
	  initializes spelling after the html message is displayed. This
	  way, all formatting is undone. Spelling is already initiated in
	  the constructor of KMComposeWin, so no harm is done deleting the
	  QTimer. BUG:106968

2006-01-13 16:59 +0000 [r497718]  vkrause

	* branches/KDE/3.5/kdepim/kresources/slox/sloxbase.cpp,
	  branches/KDE/3.5/kdepim/kresources/slox/sloxbase.h,
	  branches/KDE/3.5/kdepim/kresources/slox/kcalresourceslox.cpp,
	  branches/KDE/3.5/kdepim/kresources/slox/kabcresourceslox.cpp:
	  Read & write categories.

2006-01-13 19:38 +0000 [r497767]  bram

	* branches/KDE/3.5/kdepim/kmail/recipientspicker.cpp,
	  branches/KDE/3.5/kdepim/kmail/recipientspicker.h: Fix bug 117118:
	  composer kaddressbook no adresses at first launch Update the list
	  again once the resource is loaded. BUG:117118

2006-01-13 20:59 +0000 [r497789]  tilladam

	* branches/KDE/3.5/kdepim/kontact/plugins/test/test_plugin.cpp:
	  Don't confuse contributors with a non-compiling example. Patch by
	  Pradeepto K Bhattacharya <pradeepto@yahoo.com>

2006-01-13 23:31 +0000 [r497840]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/kalarmd/alarmdaemon.cpp: Add debug
	  output about starting KAlarm

2006-01-14 13:29 +0000 [r497977-497975]  tilladam

	* branches/KDE/3.5/kdepim/korganizer/koprefs.cpp: Forward port of:
	  SVN commit 497816 by dfaure: Let's not lie to ourselves, the
	  addressbook is loaded for any agenda-item painting. So let's load
	  it right away, not deep within painting code (which breaks in
	  case of sub-event-loops like with kio_newldap, since timers fire
	  there...)

	* branches/KDE/3.5/kdepim/kresources/kolab/kcal/resourcekolab.cpp:
	  Forward port of: SVN commit 497744 by dfaure: Stop
	  (non-single-shot!) timer before emitting the signal, god knows
	  how much time we'll spend in its slots... if we end up in a
	  subeventloop then the timer will fire repeatedly. Also committing
	  patch to avoid progress dialog in non-gui app, as discussed on
	  kdepim some time ago.

2006-01-15 07:44 +0000 [r498267]  hasso

	* branches/KDE/3.5/kdepim/kresources/kolab/kcal/resourcekolab.cpp:
	  Compile test might be helpful things, you know ... ;)

2006-01-15 15:58 +0000 [r498490]  perreaul

	* branches/KDE/3.5/kdepim/kmail/accountdialog.cpp: Backport
	  r498484.

2006-01-15 23:16 +0000 [r498704]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/librss/tools_p.cpp: fix
	  atom:content parsing: Don't show tags when for Atom 1.0 feeds
	  with escaped HTML in it BUG: 112491, 117938

2006-01-15 23:30 +0000 [r498706]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/librss/tools_p.cpp,
	  branches/KDE/3.5/kdepim/akregator/ChangeLog: fix previous commit,
	  update Changelog

2006-01-16 00:58 +0000 [r498730]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/articleviewer.cpp,
	  branches/KDE/3.5/kdepim/akregator/ChangeLog: regression: don't
	  mix up article order in combined view: sort the by date, as it
	  was in 3.4.x BUG: 118055

2006-01-16 14:39 +0000 [r498879]  dfaure

	* branches/KDE/3.5/kdepim/kdgantt/KDGanttViewSubwidgets.cpp: Avoid
	  unexpected event processing, crashes embedded kplato - see
	  koffice-devel.

2006-01-16 20:44 +0000 [r499034]  brade

	* branches/KDE/3.5/kdepim/knotes/resourcelocal.cpp: I see, the
	  problem was that in Kontact/KNotes the knotes directory wasn't
	  created. Fixed now for good. BUG: 119980 CCBUG: 116058

2006-01-16 21:13 +0000 [r499048]  brade

	* branches/KDE/3.5/kdepim/knotes/ChangeLog,
	  branches/KDE/3.5/kdepim/knotes/version.h,
	  branches/KDE/3.5/kdepim/knotes/main.cpp: After all those bugfixes
	  it's time for a shiny new version :-) (Only 10 bug reports left
	  in my list)

2006-01-16 21:53 +0000 [r499067]  brade

	* branches/KDE/3.5/kdepim/knotes/main.cpp: Oh my my... I didn't
	  notice the i18n. Sorry.

2006-01-16 22:09 +0000 [r499074]  winterz

	* branches/KDE/3.5/kdepim/libkholidays/holidays/holiday_us:
	  clean-up US holidays

2006-01-17 08:32 +0000 [r499179]  scripty

	* branches/KDE/3.5/kdepim/kmail/kmail-3.2-misc.sh,
	  branches/KDE/3.5/kdepim/kmail/kmail-3.2-update-loop-on-goto-unread-settings.sh,
	  branches/KDE/3.5/kdepim/kmail/kmail-3.3-split-sign-encr-keys.sh:
	  Increase protatbility by declaring using Bash with the help of
	  /usr/bin/env (Bug #95475) (goutte)

2006-01-17 08:46 +0000 [r499189]  scripty

	* branches/KDE/3.5/kdepim/kmail/kmail-3.2-misc.sh,
	  branches/KDE/3.5/kdepim/kmail/kmail-3.2-update-loop-on-goto-unread-settings.sh,
	  branches/KDE/3.5/kdepim/kmail/kmail-3.3-split-sign-encr-keys.sh:
	  Of course, I meant /usr/bin/env (goutte)

2006-01-17 14:45 +0000 [r499309]  vkrause

	* branches/KDE/3.5/kdepim/knode/knfolder.cpp: Backport SVN commit
	  499308 by vkrause for KDE 3.5.1: Reset busy cursor when folder
	  loading fails. BUG: 118879

2006-01-17 15:05 +0000 [r499312]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/viewer.h,
	  branches/KDE/3.5/kdepim/akregator/src/akregator_view.cpp,
	  branches/KDE/3.5/kdepim/akregator/src/akregator_view.h,
	  branches/KDE/3.5/kdepim/akregator/src/pageviewer.cpp,
	  branches/KDE/3.5/kdepim/akregator/ChangeLog,
	  branches/KDE/3.5/kdepim/akregator/src/viewer.cpp: Fix handling of
	  binary files in pageviewer. Increase the breakage in the URL
	  handling of Viewer and PageViewer. Man, I am so happy that this
	  all goes the way of the dodo in trunk. CCBUG: 120087

2006-01-18 13:21 +0000 [r499606]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/akregator_part.cpp: don't
	  backup corrupted feed list if file size is zero

2006-01-19 13:38 +0000 [r500125]  vkrause

	* branches/KDE/3.5/kdepim/kresources/remote/resourceremote.cpp:
	  Don't fail when trying to load the resource again while the
	  download is still in progress (happens eg. in Kontact's summary
	  view).

