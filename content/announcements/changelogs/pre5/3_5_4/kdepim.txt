2006-05-24 20:24 +0000 [r544438]  kloecker

	* branches/KDE/3.5/kdepim/libkdepim/kxface.cpp,
	  branches/KDE/3.5/kdepim/libkdepim/kxface.h: Disable unreachable
	  code reported by Christoph Bartoschek.

2006-05-24 21:18 +0000 [r544451]  kloecker

	* branches/KDE/3.5/kdepim/libkpgp/kpgp.cpp,
	  branches/KDE/3.5/kdepim/libkpgp/kpgpbase5.cpp,
	  branches/KDE/3.5/kdepim/libkpgp/kpgpbase6.cpp,
	  branches/KDE/3.5/kdepim/libkpgp/kpgpbase.cpp,
	  branches/KDE/3.5/kdepim/libkpgp/kpgpbase2.cpp: Fix issues
	  reported by Christoph Bartoschek. The subkey == 0 case can never
	  occur, so I've added an assert to please the code checkers.

2006-05-24 21:27 +0000 [r544453]  kloecker

	* branches/KDE/3.5/kdepim/mimelib/dwstring.cpp: pagesize will
	  always be non-zero

2006-05-24 21:39 +0000 [r544459]  kloecker

	* branches/KDE/3.5/kdepim/kmail/kmsystemtray.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmmsgpart.cpp,
	  branches/KDE/3.5/kdepim/kmail/popaccount.cpp: Add a few fall
	  throughs and one missing break. Reported by Christoph Bartoschek.

2006-05-24 21:49 +0000 [r544463]  kloecker

	* branches/KDE/3.5/kdepim/kmail/accountmanager.cpp: One false alert
	  about boolean expressions used in non-boolean contexts less.

2006-05-25 22:23 +0000 [r544730]  kloecker

	* branches/KDE/3.5/kdepim/libkdenetwork/gpgmepp/key.cpp,
	  branches/KDE/3.5/kdepim/libkdenetwork/gpgmepp/key.h: Fix two bugs
	  and implement Subkey::isSecret(). Patch by Stefan Gehn.

2006-05-26 08:26 +0000 [r544826]  kloecker

	* branches/KDE/3.5/kdepim/kmail/kmfolderseldlg.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmail.upd,
	  branches/KDE/3.5/kdepim/kmail/kmfolderseldlg.h,
	  branches/KDE/3.5/kdepim/kmail/kmstartup.cpp: Re-add the folder
	  quick filing patch now that the dialog size problems have been
	  resolved. Patch by Kumaran Santhanam. CCBUG: 113759

2006-05-27 15:57 +0000 [r545471]  kloecker

	* branches/KDE/3.5/kdepim/kmail/imapjob.cpp: Don't crash if msg is
	  0. Bug found by Akonadi.

2006-05-27 20:00 +0000 [r545580]  jriddell

	* branches/KDE/3.5/kdepim/kresources/groupwise/soap/Makefile.am:
	  Fix compilation with builddir != sourcedir see that line which
	  says " do not just set "srcdir=." here"

2006-05-28 10:41 +0000 [r545758]  kloecker

	* branches/KDE/3.5/kdepim/libkpgp/kpgp.cpp: Add a comment to make
	  clear that the loop can be executed more than once. Cf. kdepim
	  suspicious code report by Christoph Bartoschek.

2006-05-31 22:40 +0000 [r547138]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/Changelog,
	  branches/KDE/3.5/kdepim/kalarm/kalarm.h,
	  branches/KDE/3.5/kdepim/kalarm/alarmtimewidget.cpp: Fix Defer
	  dialog time interval maximum to match maximum date/time value

2006-06-01 11:43 +0000 [r547226]  tilladam

	* branches/KDE/3.5/kdepim/libkdepim/addresseelineedit.cpp:
	  Shift-tab should not allow selection either.

2006-06-01 13:00 +0000 [r547248]  dgp

	* branches/KDE/3.5/kdepim/akregator/src/mk4storage/metakit/include/mk4.h:
	  Fix building on HPPA linux, thanks to Guy Martin
	  <gmsoft@gentoo.org>

2006-06-02 21:52 +0000 [r547653]  kloecker

	* branches/KDE/3.5/kdepim/kmail/kmcomposewin.cpp: Fix bug 127538 by
	  writing config changes to disk when the composer window is
	  closed. BUG:127538

2006-06-03 00:18 +0000 [r547690]  kloecker

	* branches/KDE/3.5/kdepim/kmail/partmetadata.h: Fix bug 128513 by
	  properly initializing the creation time of a signature. In case
	  of clear signed messages we can't parse the date, so don't show
	  it. BUG:128513

2006-06-03 16:17 +0000 [r547861]  winterz

	* branches/KDE/3.5/kdepim/kmail/kmcomposewin.cpp: Don't remove
	  commas when cleaning whitespace. BUGS: 128528

2006-06-05 12:59 +0000 [r548388]  bram

	* branches/KDE/3.5/kdepim/korganizer/kogroupware.cpp: Patch from
	  John Tseng, fixing bug 127859 (Dialog box for editing or deleting
	  appointment does not catch "Yes"/No). BUG:127859

2006-06-06 22:24 +0000 [r548939]  winterz

	* branches/KDE/3.5/kdepim/konsolekalendar/main.cpp: I've been
	  wanting to uncomment this new string for a long time. and forgot
	  to do so with 3.5.3.

2006-06-09 16:02 +0000 [r549722-549721]  kainhofe

	* branches/KDE/3.5/kdepim/libkholidays/kholidays.h,
	  branches/KDE/3.5/kdepim/libkholidays/scanholiday.lex,
	  branches/KDE/3.5/kdepim/libkholidays/kholidays.cpp,
	  branches/KDE/3.5/kdepim/libkholidays/Makefile.am,
	  branches/KDE/3.5/kdepim/libkholidays/parseholiday.y: The holiday
	  library now also supports -) Multiple holidays on the same date
	  (e.g. start of summer time and easter on the same day, etc.) -)
	  conditional shifting of holidays (e.g. if a holiday occurs on a
	  saturday or sunday, it is actually celebrated on the following
	  monday). Example: small "May holiday" weekend on 1.5. shift to
	  monday if saturday || sunday The holiday data files still need to
	  be adapted wherever such a system applies (UK, Wales, Ireland are
	  known from bug reports, but I don't know which holidays are
	  really affected). It's incredible, how simple it is to hack these
	  lex/yacc parsers (which had the fame that nobody in kde would
	  understand them), once you take the ten minutes to print out a
	  yacc tutorial and the 45 minutes to look at the code (and then
	  the ~2 hours to fix the inadvertant bugs in the C code that come
	  through the ancient pointer-increasing in C to walk through a
	  list, when you are used to Qt-style list iterators ;-) )
	  CCBUG:64673 CCBUG:92474

	* branches/KDE/3.5/kdepim/libkholidays/scanholiday.c,
	  branches/KDE/3.5/kdepim/libkholidays/parseholiday.c,
	  branches/KDE/3.5/kdepim/libkholidays/parseholiday.h: Re-generate
	  the yacc parser and the lex parser from the modified .y and .lex
	  files

2006-06-09 16:31 +0000 [r549733]  kainhofe

	* branches/KDE/3.5/kdepim/kontact/plugins/specialdates/sdsummarywidget.cpp,
	  branches/KDE/3.5/kdepim/korganizer/kocorehelper.cpp,
	  branches/KDE/3.5/kdepim/korganizer/koglobals.h,
	  branches/KDE/3.5/kdepim/korganizer/koagendaview.cpp,
	  branches/KDE/3.5/kdepim/korganizer/actionmanager.cpp,
	  branches/KDE/3.5/kdepim/korganizer/komonthview.cpp,
	  branches/KDE/3.5/kdepim/korganizer/kodaymatrix.cpp,
	  branches/KDE/3.5/kdepim/libkcal/htmlexport.cpp,
	  branches/KDE/3.5/kdepim/korganizer/koglobals.cpp: Implement
	  support for the new features in libkholiday: -) Multiple holidays
	  are possible on a day (e.g. start of summer time and easter)
	  BUG:92474

2006-06-09 21:57 +0000 [r549799]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/recurrenceedit.cpp,
	  branches/KDE/3.5/kdepim/kalarm/Changelog,
	  branches/KDE/3.5/kdepim/kalarm/alarmevent.cpp: Fix crash when a
	  deferred expired recurring alarm is edited from message window
	  Edit button

2006-06-10 18:59 +0000 [r550047]  brade

	* branches/KDE/3.5/kdepim/knotes/knote.cpp,
	  branches/KDE/3.5/kdepim/knotes/knote.h: Reducing the patch sent
	  to k-c-d: - move setColor() to another position, make it public -
	  add const accessors for fgColor and bgColor No functional change.

2006-06-11 16:47 +0000 [r550406]  deller

	* branches/KDE/3.5/kdepim/kaddressbook/xxport/gnokii_xxport.cpp:
	  Add additional support for the next upcoming gnokii version (>=
	  0.6.13) All versions below that number are not binary compatible,
	  which means, that users could never upgrade gnokii without
	  recompiling this import/export filter. With 0.6.13 this should be
	  fixed. No new strings added, just many duplicates.

2006-06-11 23:50 +0000 [r550490]  brade

	* branches/KDE/3.5/kdepim/knotes/ChangeLog,
	  branches/KDE/3.5/kdepim/knotes/knotesapp.h,
	  branches/KDE/3.5/kdepim/knotes/knote.cpp,
	  branches/KDE/3.5/kdepim/knotes/KNotesAppIface.h,
	  branches/KDE/3.5/kdepim/knotes/knotesapp.cpp: Add DCOP accessors
	  and setters for the note colors. The signal sigDataChanged() has
	  to be emitted instead of calling saveData() directly to prevent
	  the CTOR from saving the yet empty notes back to disk---at that
	  time the signal is not connected yet; additionally, saveData()
	  would call Journal::setDescription() with an empty editor,
	  effectively deleting the note before it was even read at all.
	  Journal::updated() has to be called to tell the resource that the
	  data has changed. setCustomProperty() does not do that, thus the
	  color changes wouldn't be saved to disk if nothing else was
	  changed. CCMAIL: caio1982@gmail.com

2006-06-12 19:35 +0000 [r550796]  kloecker

	* branches/KDE/3.5/kdepim/kmail/kmmessage.cpp: Fix SVN commit
	  519171. This should be compared to 'name*' as before. The '*'
	  here is not a placeholder.

2006-06-12 23:00 +0000 [r550851]  kloecker

	* branches/KDE/3.5/kdepim/kmail/newfolderdialog.cpp: Fix bug 120448
	  (Crash when creating a mail folder with a / in its name)
	  BUG:120448

2006-06-15 16:39 +0000 [r551807]  mlaurent

	* branches/KDE/3.5/kdepim/kresources/egroupware/debugdialog.cpp:
	  Add missing i18n

2006-06-15 16:42 +0000 [r551809]  mlaurent

	* branches/KDE/3.5/kdepim/kresources/egroupware/debugdialog.cpp:
	  Add #include <klocale.h>

2006-06-16 17:18 +0000 [r552126]  burghard

	* branches/KDE/3.5/kdepim/kmail/kmfolderimap.cpp: There was an open
	  missing which lead to the famous chain of horror: the parent
	  folder was not open so the job opened it, the messagelist was
	  regenerated, the message became invalid, yada yada. BUGS: 129028

2006-06-17 01:10 +0000 [r552219-552218]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/messagewin.cpp: Bug 129024: fix
	  crash when message is redisplayed at login

	* branches/KDE/3.5/kdepim/kalarm/Changelog: Bug 129024: fix crash
	  when message is redisplayed at login

2006-06-17 22:01 +0000 [r552469]  kloecker

	* branches/KDE/3.5/kdepim/kmail/searchjob.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmacctimap.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmfolderimap.cpp: Fix some word
	  puzzles and one untranslated string.

2006-06-18 08:54 +0000 [r552548]  burghard

	* branches/KDE/3.5/kdepim/kmail/renamejob.cpp: It is too risky to
	  delete the new folder in case the undo did not work

2006-06-19 00:44 +0000 [r552758]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/messagewin.cpp,
	  branches/KDE/3.5/kdepim/kalarm/Changelog: Prevent inapplicable
	  'Unable to speak' error when alarm is redisplayed after login

2006-06-19 16:57 +0000 [r553004]  tokoe

	* branches/KDE/3.5/kdepim/kresources/egroupware/kcal_resourcexmlrpc.cpp:
	  Change the limit to a value for daily use...

2006-06-19 22:28 +0000 [r553081]  djarvie

	* branches/KDE/3.5/kdepim/doc/kalarm/index.docbook: Minor
	  corrections

2006-06-20 12:38 +0000 [r553215]  wstephens

	* branches/KDE/3.5/kdepim/korganizer/koeditorrecurrence.cpp: Fix
	  off by one error in yearly recurrence. Patch by Daniel Gollub
	  (dgollub@suse.de).

2006-06-20 19:40 +0000 [r553344]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/Changelog,
	  branches/KDE/3.5/kdepim/kalarm/alarmcalendar.cpp,
	  branches/KDE/3.5/kdepim/kalarm/kalarmui.rc,
	  branches/KDE/3.5/kdepim/doc/kalarm/index.docbook,
	  branches/KDE/3.5/kdepim/kalarm/alarmcalendar.h,
	  branches/KDE/3.5/kdepim/kalarm/kalarm.h,
	  branches/KDE/3.5/kdepim/kalarm/mainwindow.cpp,
	  branches/KDE/3.5/kdepim/kalarm/mainwindow.h: New feature: Add
	  facility to import alarms from other calendar files Approved by
	  Cornelius Schumacher
	  (http://lists.kde.org/?l=kde-core-devel&m=115075382309643&w=2)
	  New strings approved
	  http://lists.kde.org/?l=kde-i18n-doc&m=115080404913032&w=2 ** New
	  translatable strings are backported from trunk ** CCMAIL:
	  kde-i18n-doc@kde.org

2006-06-20 21:02 +0000 [r553370]  kloecker

	* branches/KDE/3.5/kdepim/kmail/kmmainwidget.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmmainwidget.h: Fix bug 124501
	  (Some keyboard shortcuts bound twice) and 128984 ("Jump to
	  folder" kbd shortcut hardcoded). Don't use a QAccel. For one we
	  already have an appropriate KAccel
	  (actioncollection()->kaccel()). Moreover, using QAccel breaks the
	  configurability of kbd shortcuts. KAction->plugAccel is
	  deprecated but I don't see what else to use. I guess the
	  additional plugAccel shouldn't be necessary, but at least for
	  Return the accel didn't work out of the box (probably because the
	  corresponding KAction doesn't appear in any menu or toolbar).
	  BUGS: 124501 CCBUGS: 128984

2006-06-21 08:54 +0000 [r553500]  mkelder

	* branches/KDE/3.5/kdepim/korn/main.cpp,
	  branches/KDE/3.5/kdepim/korn/subjectsdlg.cpp: - Changed email
	  address. - Corrected the making of a list of messages which have
	  to be deleted for a given account. This will fix multiple
	  messages removal from one server. BUG: 128361

2006-06-21 21:56 +0000 [r553736]  kloecker

	* branches/KDE/3.5/kdepim/kmail/headerstyle.cpp: Fix bug 129554
	  (all-headers display should always use direction=ltr) Patch by
	  Chen Levy. Thanks. BUG: 129554

2006-06-22 05:42 +0000 [r553786]  burghard

	* branches/KDE/3.5/kdepim/kmail/kmcommands.h,
	  branches/KDE/3.5/kdepim/kmail/kmcommands.cpp: When you move
	  messages from an imap to a local folder you get the completed
	  signal way to early. This broke moving of folders as the source
	  folder was removed too early. This fixes it for me.

2006-06-23 22:52 +0000 [r554389]  kloecker

	* branches/KDE/3.5/kdepim/kmail/messagecomposer.cpp,
	  branches/KDE/3.5/kdepim/kmail/messagecomposer.h: Fix bug 129662
	  (After upgrading from 3.5.2 to 3.5.3 when I send encrypted
	  messages, the new sent messages in sent mailbox are no longer
	  parsable by kmail. Encrypt to self is turned on, but but mailer
	  doesn't see the message in the multipart/mixed body) Also save
	  the mp/mixed boundary so that we can later set it. Checked with
	  signed/unsigned encrypted/unencrypted plain/HTML messages
	  with/without attachments. BUGS:129662

2006-06-25 18:47 +0000 [r554926]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/pageviewer.cpp,
	  branches/KDE/3.5/kdepim/akregator/ChangeLog,
	  branches/KDE/3.5/kdepim/akregator/src/viewer.cpp: fix browser tab
	  context menus, including copy action when text is selected. These
	  flags seriously need a cleanup so one can figure out what to do
	  without reading khtmlpart code... BUG: 121957

2006-06-25 21:40 +0000 [r554970]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/pageviewer.cpp: eh, set
	  these bools correctly. stupid me.

2006-06-28 07:16 +0000 [r555658]  gungl

	* branches/KDE/3.5/kdepim/kmail/kmail.antispamrc,
	  branches/KDE/3.5/kdepim/kmail/kmheaders.cpp,
	  branches/KDE/3.5/kdepim/kmail/kmcommands.cpp: - anti-spam wizard
	  support for BsFilter (from Yuya Nishihara <yuya at tcha dot org>)
	  - anti-spam wizard support for DSpam (from Kovid Goyal
	  <kovid@theory.caltech.edu>) - better feedback during filtering,
	  msg count is visible in the statusbar FEATURE: 115287 FEATURE:
	  128060 GUI:

2006-06-28 15:01 +0000 [r555779]  mlaurent

	* branches/KDE/3.5/kdepim/korganizer/koeditoralarms.cpp: Initialize
	  variable to avoid crash

2006-07-02 10:01 +0000 [r557052]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/alarmcalendar.cpp,
	  branches/KDE/3.5/kdepim/kalarm/alarmcalendar.h: Fix UTF-8
	  encoding

2006-07-05 20:06 +0000 [r558673]  kloecker

	* branches/KDE/3.5/kdepim/kmail/kmaccount.cpp,
	  branches/KDE/3.5/kdepim/kmail/popaccount.cpp: Fix progress item
	  related crashes during POP3 fetches. BUGS: 110487, 118112,
	  119112, 121384, 127210, 130303

2006-07-07 18:16 +0000 [r559595]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/ChangeLog,
	  branches/KDE/3.5/kdepim/akregator/src/progressmanager.cpp: fix
	  possible crash BUG: 130377

2006-07-07 19:08 +0000 [r559615]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/akregator_part.cpp,
	  branches/KDE/3.5/kdepim/akregator/src/librss/tools_p.cpp,
	  branches/KDE/3.5/kdepim/akregator/ChangeLog: remove tray icon
	  when disabling akregator component in kontact BUG: 130127

2006-07-09 12:01 +0000 [r560150]  lueck

	* branches/KDE/3.5/kdepim/doc/knode/commands.docbook: documentation
	  backport from trunk CCMAIL:kde-doc-english

2006-07-10 09:26 +0000 [r560400]  tilladam

	* branches/KDE/3.5/kdepim/kmail/kmfolderimap.cpp: Forward port of:
	  SVN commit 560397 by tilladam: Make sure that flags changes from
	  the server are merged both ways, flag removal and flag setting.
	  (Proko2 issue 1299)

2006-07-10 20:01 +0000 [r560629-560626]  brade

	* branches/KDE/3.5/kdepim/knotes/knotesapp.h,
	  branches/KDE/3.5/kdepim/knotes/KNotesAppIface.h,
	  branches/KDE/3.5/kdepim/knotes/knotesapp.cpp: Add 4 new DCOP
	  functions to get and change a note's geometry: width(), height(),
	  move(), resize() Ok'ed by Cornelius.

	* branches/KDE/3.5/kdepim/knotes/ChangeLog: Forgot to commit this
	  file, add an entry about the new geometry DCOP functions.

2006-07-10 20:58 +0000 [r560665]  burghard

	* branches/KDE/3.5/kdepim/kmail/kmfolderimap.cpp: This should help
	  a bit with duplicated messages

2006-07-11 21:36 +0000 [r561333]  mueller

	* branches/KDE/3.5/kdepim/kalarm/soundpicker.cpp: fix compile
	  --without-arts

2006-07-11 22:46 +0000 [r561356]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/alarmlistview.cpp,
	  branches/KDE/3.5/kdepim/kalarm/Changelog,
	  branches/KDE/3.5/kdepim/kalarm/alarmlistview.h,
	  branches/KDE/3.5/kdepim/kalarm/mainwindow.cpp,
	  branches/KDE/3.5/kdepim/kalarm/mainwindow.h: Remember column
	  ordering in main window between sessions, in case the user
	  reorders the columns. (Except for the message column, which is
	  always set to the last column.) Prevent the alarm type column
	  from being resized, to prevent refresh problems. BUG: 130298 BUG:
	  130297

2006-07-11 23:16 +0000 [r561360]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/mainwindow.cpp,
	  branches/KDE/3.5/kdepim/kalarm/mainwindow.h: Fix UTF-8 encoding

2006-07-11 23:49 +0000 [r561368]  djarvie

	* branches/KDE/3.5/kdepim/kalarm/messagewin.cpp,
	  branches/KDE/3.5/kdepim/kalarm/dcophandler.cpp,
	  branches/KDE/3.5/kdepim/kalarm/kalarmiface.h,
	  branches/KDE/3.5/kdepim/kalarm/main.cpp,
	  branches/KDE/3.5/kdepim/kalarm/undo.cpp,
	  branches/KDE/3.5/kdepim/kalarm/dcophandler.h,
	  branches/KDE/3.5/kdepim/kalarm/functions.cpp,
	  branches/KDE/3.5/kdepim/kalarm/kalarmapp.cpp,
	  branches/KDE/3.5/kdepim/kalarm/ACKNOWLEDGEMENTS,
	  branches/KDE/3.5/kdepim/kalarm/kalarm.h,
	  branches/KDE/3.5/kdepim/kalarm/functions.h,
	  branches/KDE/3.5/kdepim/kalarm/kalarmapp.h: Fix UTF-8 encoding

2006-07-12 15:45 +0000 [r561585]  kainhofe

	* branches/KDE/3.5/kdepim/libkholidays/holidays/holiday_se,
	  branches/KDE/3.5/kdepim/libkholidays/holidays/holiday_au: Fix
	  holidays files

2006-07-14 11:25 +0000 [r562217]  wstephens

	* branches/KDE/3.5/kdepim/kresources/groupwise/soap/incidenceconverter.cpp:
	  Fix for crash when writing appointments.

2006-07-19 12:54 +0000 [r564166]  mueller

	* branches/KDE/3.5/kdepim/kmail/kmkernel.cpp: fix crash on logout
	  when only a message pane is shown. BUG: 192416

2006-07-20 12:07 +0000 [r564528]  kling

	* branches/KDE/3.5/kdepim/kandy/src/mobilegui.cpp: Fixed a crash in
	  "Set Clock" function. Thanks to Maciej Witkowiak for report &
	  fix. BUG: 104908

2006-07-20 12:43 +0000 [r564537]  kling

	* branches/KDE/3.5/kdepim/kandy/src/modem.cpp: Fixed the well-known
	  problem with garbled device paths. Patch has been lying around on
	  bugzilla for over a year. BUG: 39420 BUG: 45640 BUG: 47383 BUG:
	  96883 BUG: 104596 BUG: 101575

2006-07-20 20:01 +0000 [r564676]  kloecker

	* branches/KDE/3.5/kdepim/kmail/kmcomposewin.cpp: Fix bug 131119
	  (Sending attachments with umlauts in the filename breaks rfc2231)
	  rfc2231-encoded parameter values must not be enclosed in double
	  quotes. BUG: 131119

2006-07-20 20:54 +0000 [r564686]  burghard

	* branches/KDE/3.5/kdepim/kmail/kmcomposewin.cpp: Compile

2006-07-22 10:01 +0000 [r565057]  tstaerk

	* branches/KDE/3.5/kdepim/karm/mainwindow.cpp: Make s start and
	  stop timing. BUGS:131179

2006-07-22 10:49 +0000 [r565075]  osterfeld

	* branches/KDE/3.5/kdepim/akregator/src/aboutdata.h: bump version

2006-07-22 11:13 +0000 [r565083]  vkrause

	* branches/KDE/3.5/kdepim/knode/resource.h: increment version
	  number

2006-07-22 17:28 +0000 [r565201]  kloecker

	* branches/KDE/3.5/kdepim/kmail/kmversion.h: KDE 3.5.4 peeks around
	  the corner

2006-07-23 13:06 +0000 [r565426]  winterz

	* branches/KDE/3.5/kdepim/kresources/groupwise/soap/stdsoap2.cpp,
	  branches/KDE/3.5/kdepim/kresources/groupwise/soap/stdsoap2.h:
	  Re-commit this fix for bug #123214. Apparently the big merge from
	  Revision 542151 ("Merge GW701 branch back to 3_5 branch. This is
	  compatible with GroupWise 7SP1.") undid the fix. Sorry about
	  this. CCMAIL: wstephenson@kde.org BUGS: 123214

2006-07-23 14:02 +0000 [r565475]  coolo

	* branches/KDE/3.5/kdepim/kdepim.lsm: preparing KDE 3.5.4

2006-07-23 21:00 +0000 [r565605]  burghard

	* branches/KDE/3.5/kdepim/kmail/kmfolderimap.cpp: Revert Till's fix
	  as it breaks the detection of read messages. BUGS: 131098 CCMAIL:
	  adam@kde.org

