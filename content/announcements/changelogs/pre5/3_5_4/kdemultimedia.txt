2006-05-27 11:08 +0000 [r545339]  coolo

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/ripper.cpp: fix
	  crash (CID 2278)

2006-05-27 11:58 +0000 [r545364]  mueller

	* branches/KDE/3.5/kdemultimedia/kaudiocreator/ripper.cpp: don't
	  use dynamic_cast if you're not checking the result anyway (CID
	  2267)

2006-05-29 08:20 +0000 [r546121]  coolo

	* branches/KDE/3.5/kdemultimedia/kioslave/audiocd/audiocd.cpp:
	  pretty hard to tell if other platforms have the same struct
	  members ;( At least FreeBSD hasn't, so I have to assume it's
	  linux specific

2006-05-29 12:10 +0000 [r546178]  mueller

	* branches/KDE/3.5/kdemultimedia/libkcddb/kcmcddb/cddbconfigwidget.cpp:
	  fix integer conversion check

2006-06-12 13:49 +0000 [r550637]  mueller

	* branches/KDE/3.5/kdemultimedia/kscd/libwm/cddaslave.c,
	  branches/KDE/3.5/kdemultimedia/kscd/cddaslave.c: add return value
	  checks

2006-06-12 22:00 +0000 [r550832]  carewolf

	* branches/KDE/3.5/kdemultimedia/akode_artsplugin/akodePlayObject_impl.cpp:
	  Commit my personal changes, so I am not the only one with an
	  artsd that doesn't leak *shame on me* BUG:119504

2006-06-13 04:37 +0000 [r550906]  mpyne

	* branches/KDE/3.5/kdemultimedia/akode_artsplugin/akodePlayObject_impl.cpp:
	  Make the memory leak fix compile. CCBUG:119504

2006-06-22 22:45 +0000 [r554030]  mueller

	* branches/KDE/3.5/kdemultimedia/kmix/kmixprefdlg.cpp: formatting
	  fix (under message unfreeze rule)

2006-07-20 15:56 +0000 [r564612]  helio

	* branches/KDE/3.5/kdemultimedia/akode_artsplugin/Makefile.am,
	  branches/KDE/3.5/kdemultimedia/xine_artsplugin/Makefile.am:
	  Sometimes arts goes up 100% cpu and we don't know why :-/

2006-07-20 17:12 +0000 [r564639]  helio

	* branches/KDE/3.5/kdemultimedia/audiofile_artsplugin/Makefile.am:
	  - Why we have so many of the same type of modules with different
	  name schemas ? And again, the same no-version problem, as
	  previous xine and akode plugins

2006-07-21 14:47 +0000 [r564851]  helio

	* branches/KDE/3.5/kdemultimedia/akode_artsplugin/Makefile.am: -
	  Include artsbuilder is wrong here. akode plugin now links against
	  artsflow.

2006-07-23 14:02 +0000 [r565473]  coolo

	* branches/KDE/3.5/kdemultimedia/kdemultimedia.lsm: preparing KDE
	  3.5.4

2006-07-24 10:06 +0000 [r565716]  coolo

	* branches/KDE/3.5/kdemultimedia/audiofile_artsplugin/audiofilePlayObject.mcopclass:
	  excellent breackage Helio

