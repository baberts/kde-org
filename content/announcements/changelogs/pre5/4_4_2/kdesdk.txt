------------------------------------------------------------------------
r1098400 | lueck | 2010-03-04 05:57:10 +1300 (Thu, 04 Mar 2010) | 1 line

doc backport from trunk
------------------------------------------------------------------------
r1099446 | lueck | 2010-03-06 04:39:32 +1300 (Sat, 06 Mar 2010) | 1 line

kate has no simple mode
------------------------------------------------------------------------
r1103417 | scripty | 2010-03-15 15:32:49 +1300 (Mon, 15 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1104917 | shaforo | 2010-03-19 11:03:45 +1300 (Fri, 19 Mar 2010) | 6 lines

BUG: 230365
temporary fix for 'Lokalize crash when i remove an entry from glossary' bug.

I'm rewriting glossary storage for trunk nowadays


------------------------------------------------------------------------
r1104933 | shaforo | 2010-03-19 12:04:34 +1300 (Fri, 19 Mar 2010) | 3 lines

BUG: 229678
'If I close all windows within Lokalize, then close Lokalize it segfaults'

------------------------------------------------------------------------
r1105825 | scripty | 2010-03-22 02:22:20 +1300 (Mon, 22 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1106886 | scripty | 2010-03-24 15:28:48 +1300 (Wed, 24 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1107213 | scripty | 2010-03-25 15:29:49 +1300 (Thu, 25 Mar 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1107598 | dhaumann | 2010-03-26 19:54:42 +1300 (Fri, 26 Mar 2010) | 5 lines

backport: fix splitter orientation in ::removeViewSpace

If a viewspace in the middle is deleted, we can move all widgets one level
up and eliminate one redundant splitter. We have to make sure the removed
splitter's orientation is kept. This is what the fix is about...
------------------------------------------------------------------------
