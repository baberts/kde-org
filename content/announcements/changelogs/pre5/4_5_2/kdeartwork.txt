------------------------------------------------------------------------
r1179170 | annma | 2010-09-25 06:09:39 +1200 (Sat, 25 Sep 2010) | 3 lines

backport 1179036: allow exif rotation when libkexiv2 is present
BUG=231599

------------------------------------------------------------------------
r1179293 | scripty | 2010-09-25 14:46:35 +1200 (Sat, 25 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1179693 | scripty | 2010-09-26 16:11:31 +1300 (Sun, 26 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1180448 | scripty | 2010-09-28 16:22:28 +1300 (Tue, 28 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1181097 | mpyne | 2010-09-30 13:51:54 +1300 (Thu, 30 Sep 2010) | 7 lines

Fix a bug in KDE Asciiquarium with the "Nessie" animation, where the creature would
delete the water underneath her in KDE 4.5.2 (if I made it in time for tagging).

Caught and fixed by Ryan Meldrum.

CCMAIL:ryjame@cox.net

------------------------------------------------------------------------
