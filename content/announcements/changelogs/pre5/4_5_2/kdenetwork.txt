------------------------------------------------------------------------
r1168852 | lueck | 2010-08-28 03:34:35 +1200 (Sat, 28 Aug 2010) | 1 line

add missing i18n call
------------------------------------------------------------------------
r1170130 | mfuchs | 2010-08-31 08:19:47 +1200 (Tue, 31 Aug 2010) | 4 lines

Backports r1170129 r1170122 r1170079
Caches the mimeType icons in the model for some speed gains.
Especially noticeable with many Transfers.
Also caches the icon for the mimeType in the FileModel.
------------------------------------------------------------------------
r1170567 | mfuchs | 2010-09-02 00:52:18 +1200 (Thu, 02 Sep 2010) | 2 lines

Backport r1170566
Speeds up loading of KGet by avoiding to set the Nepomuk settings of finished transfers again.
------------------------------------------------------------------------
r1170590 | mfuchs | 2010-09-02 02:21:24 +1200 (Thu, 02 Sep 2010) | 2 lines

Backport r1170589
Speeds removing of transfers up, by trying to remove multiple rows at once and thus not constantly updating the view.
------------------------------------------------------------------------
r1170594 | mfuchs | 2010-09-02 02:38:27 +1200 (Thu, 02 Sep 2010) | 1 line

Remove unneded method introduced with last commit.
------------------------------------------------------------------------
r1171056 | sitter | 2010-09-03 03:07:40 +1200 (Fri, 03 Sep 2010) | 4 lines

Backport r1171055.

Add WITH_WLM_MEDIASTREAMER option to allow our friends from Kubuntu to build WLM without linking to mediastreamer.

------------------------------------------------------------------------
r1171380 | fschaefer | 2010-09-04 03:36:38 +1200 (Sat, 04 Sep 2010) | 6 lines

Fix bug that could cause device switching (on removal of the current video device) to fail

The way QVector organizes its content can cause the destructors of the
videodevice objects to be called (even when they are not deleted).
Closing the video device in the destructor will therefore cause trouble.

------------------------------------------------------------------------
r1171824 | qbast | 2010-09-05 22:46:16 +1200 (Sun, 05 Sep 2010) | 6 lines

Backport of bugfix for 205992

Use grabWindow to obtain screenshot of embedded rdesktop window
CCBUG: 205992


------------------------------------------------------------------------
r1172692 | lvsouza | 2010-09-08 08:56:26 +1200 (Wed, 08 Sep 2010) | 10 lines

Backport 1170480 and 1172124 by lvsouza from trunk to the 4.5 branch:

1170480: Use icon path directly with KStatusNotifierItem instead of using img src=\"kopete-account-icon:%3:%4\"
since kopete-account-icon does not work with KStatusNotifierItem.
CCBUG: 222689

1172124: System tray tooltip: sort accounts by online status to make it easier to read
their status when user has several accounts.


------------------------------------------------------------------------
r1172872 | scripty | 2010-09-08 15:22:12 +1200 (Wed, 08 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1173049 | mlaurent | 2010-09-09 01:38:17 +1200 (Thu, 09 Sep 2010) | 3 lines

Backport fix 250451
BUG: 250451

------------------------------------------------------------------------
r1173956 | mfuchs | 2010-09-11 11:28:42 +1200 (Sat, 11 Sep 2010) | 3 lines

Backport r1172084
Removes TransferTreeModel::removeTransfer and also makes all remove signals use lists of transfers, this especially speeds up the dbus signals.
When deleting a group from the model the transfers of that group are removed in the same method of the model.
------------------------------------------------------------------------
r1173957 | mfuchs | 2010-09-11 11:28:45 +1200 (Sat, 11 Sep 2010) | 3 lines

Backport r1172085
Adds KGet::createTransfers and TransferTreeModel::addTransfers to speed up the loading process of KGet.
Removes TransferTreeModel::addTransfer and also makes all add signals use lists of transfers, this especially speeds up the dbus signals.
------------------------------------------------------------------------
r1173958 | mfuchs | 2010-09-11 11:28:48 +1200 (Sat, 11 Sep 2010) | 3 lines

Backport r1172150
Do not call DataSourceFactory::changeStatus from DataSourceFactory::load since some signals were called twice that way, do that instead in load directly.
Removes unneeded C-cast and combines some lines.
------------------------------------------------------------------------
r1173959 | mfuchs | 2010-09-11 11:28:49 +1200 (Sat, 11 Sep 2010) | 2 lines

Backport r1172693
Adds a TransferHistoryStore::delTransfers and uses this in the SQLiteStore in connection with a transaction. The later reduces the amount of io operations and thus speeds up the process a lot.
------------------------------------------------------------------------
r1173960 | mfuchs | 2010-09-11 11:28:51 +1200 (Sat, 11 Sep 2010) | 1 line

Do not show the "new transfer added" notification during startup process, since those aren't new transfers.
------------------------------------------------------------------------
r1173961 | mfuchs | 2010-09-11 11:28:53 +1200 (Sat, 11 Sep 2010) | 2 lines

Backport r1172721
Only add a '*' to wildcards if none was defined.
------------------------------------------------------------------------
r1173962 | mfuchs | 2010-09-11 11:28:54 +1200 (Sat, 11 Sep 2010) | 2 lines

Backport r1172723
Transfer checks if capabilities have changed, before emitting a signal.
------------------------------------------------------------------------
r1173963 | mfuchs | 2010-09-11 11:28:56 +1200 (Sat, 11 Sep 2010) | 1 line

Adds TransferGroup::append(const QList<Transfer*> transfers) and TransferGroup::remove(const QList<Transfer*> transfers). Avoids constant rescheduling.
------------------------------------------------------------------------
r1173964 | mfuchs | 2010-09-11 11:28:58 +1200 (Sat, 11 Sep 2010) | 4 lines

Backport r1173954
Inlines JobQueue iterator methods and some other methods.
TransferGroup::supportsSpeedLimits returns false if there are no running jobs.
Removes unneded code.
------------------------------------------------------------------------
r1174638 | scripty | 2010-09-13 14:37:46 +1200 (Mon, 13 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1175054 | murrant | 2010-09-14 13:04:57 +1200 (Tue, 14 Sep 2010) | 8 lines

Backport r1175043 by murrant from trunk to the 4.5 branch:

Fix the focus problem by re-ordering the widget creation.

Disable selections on the connection list.
CCBUG: 249789


------------------------------------------------------------------------
r1176400 | poboiko | 2010-09-18 03:13:54 +1200 (Sat, 18 Sep 2010) | 5 lines

Backport commit 1176365
Open hotmail inbox using default web browser (not a default html viewer)
CCBUG: 225043


------------------------------------------------------------------------
r1176919 | scripty | 2010-09-19 14:47:53 +1200 (Sun, 19 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1177576 | mfuchs | 2010-09-21 04:10:37 +1200 (Tue, 21 Sep 2010) | 2 lines

Only creates the target folder if a download is started.
BUG:251441
------------------------------------------------------------------------
r1177733 | murrant | 2010-09-21 15:08:25 +1200 (Tue, 21 Sep 2010) | 5 lines

Backport r1177173 by murrant from trunk to the 4.5 branch:

Replace KUrlNavigator with a KComboBox and a KLineEdit.  This patch is intended to be backported to 4.5.


------------------------------------------------------------------------
r1177736 | murrant | 2010-09-21 15:13:38 +1200 (Tue, 21 Sep 2010) | 5 lines

Backport r1177731 by murrant from trunk to the 4.5 branch:

Do not print out the arguments with kDebug.


------------------------------------------------------------------------
r1179182 | mfuchs | 2010-09-25 06:52:26 +1200 (Sat, 25 Sep 2010) | 2 lines

Backport r1177579
HACK: Suspends the scheduler when starting/stopping selected transfers as it would constantly reschedule otherwise slowing down the process a lot.
------------------------------------------------------------------------
r1179697 | scripty | 2010-09-26 16:14:01 +1300 (Sun, 26 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1180451 | scripty | 2010-09-28 16:24:57 +1300 (Tue, 28 Sep 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
