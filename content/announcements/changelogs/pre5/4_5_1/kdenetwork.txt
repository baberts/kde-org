------------------------------------------------------------------------
r1165284 | rytilahti | 2010-08-18 19:40:45 +0100 (dc, 18 ago 2010) | 7 lines

backport r1156229:

Use KIO::encodeFilename() before trying to save the user's avatar photo to the disk. This is needed because in XMPP the JID may contain a slash, which is reserved character in filesystem.

CCBUGS:156184


------------------------------------------------------------------------
r1165283 | rytilahti | 2010-08-18 19:37:38 +0100 (dc, 18 ago 2010) | 7 lines

backport r1156102:

Don't show joins&quits in chatwindow, if user has decided not to show events

CCBUG:109822


------------------------------------------------------------------------
r1165233 | mfuchs | 2010-08-18 16:57:01 +0100 (dc, 18 ago 2010) | 4 lines

Do not stop the Schedulers when quitting. That way the Policy of the TransferGroups remains unchanged.
That means that Transfers don't get stopped now before they are destroyed.
Might be related to
CCBUG:179843
------------------------------------------------------------------------
r1164729 | gberg | 2010-08-17 15:53:12 +0100 (dt, 17 ago 2010) | 6 lines

Backport r1164726: Fix build on Solaris due to missing include.

Thanks to tropikhajma for the patch.

CCBUG:243863

------------------------------------------------------------------------
r1164724 | gberg | 2010-08-17 15:42:24 +0100 (dt, 17 ago 2010) | 6 lines

Backport r1164722: Fix shift modifier key not working when pressed on remote keyboard.

Patch by Dariusz Mikulski.

CCBUG:182073

------------------------------------------------------------------------
r1163288 | mfuchs | 2010-08-13 16:57:16 +0100 (dv, 13 ago 2010) | 3 lines

Backport r1163286
Do not divide by zero. Happens after starting a download with an empty download list.
CCBUG:246668
------------------------------------------------------------------------
r1163240 | mfuchs | 2010-08-13 16:22:51 +0100 (dv, 13 ago 2010) | 3 lines

Moves getting of sessionBus to init() since that runs in the mainthread (thx aseigo). Should work around a DBus issue that caused a crash.
Includes some whitespace changes.
BUG:221751
------------------------------------------------------------------------
r1161757 | mfuchs | 2010-08-10 20:05:25 +0100 (dt, 10 ago 2010) | 2 lines

Respect the maximum file size limit of VFAT.
CCBUG:245623
------------------------------------------------------------------------
r1161728 | mfuchs | 2010-08-10 18:57:49 +0100 (dt, 10 ago 2010) | 1 line

Reverts wrong commit r1161727 "not working!"
------------------------------------------------------------------------
r1161727 | mfuchs | 2010-08-10 18:55:42 +0100 (dt, 10 ago 2010) | 2 lines

Checks if the putjob deletes itself.
BUG:245589
------------------------------------------------------------------------
r1161726 | mfuchs | 2010-08-10 18:55:33 +0100 (dt, 10 ago 2010) | 1 line

not working!
------------------------------------------------------------------------
