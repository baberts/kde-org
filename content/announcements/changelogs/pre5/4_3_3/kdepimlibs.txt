------------------------------------------------------------------------
r1030495 | mueller | 2009-10-02 11:46:41 +0000 (Fri, 02 Oct 2009) | 2 lines

bump version

------------------------------------------------------------------------
r1031598 | tnyblom | 2009-10-05 13:44:58 +0000 (Mon, 05 Oct 2009) | 7 lines

Backport r1028946 by tnyblom from trunk to the 4.3 branch:

This makes KMime allow a trailing dot in domain names as per the RFC.

CCBUG: 139477


------------------------------------------------------------------------
r1031785 | scripty | 2009-10-06 03:00:14 +0000 (Tue, 06 Oct 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1033301 | tmcguire | 2009-10-09 21:13:53 +0000 (Fri, 09 Oct 2009) | 21 lines

Backport r1033254 by tmcguire from trunk to the 4.3 branch:

Merged revisions 1030030 via svnmerge from 
svn+ssh://tmcguire@svn.kde.org/home/kde/branches/kdepim/enterprise4/kdepim

................
  r1030030 | winterz | 2009-10-01 14:16:59 +0200 (Thu, 01 Oct 2009) | 11 lines
  
  Merged revisions 1030016 via svnmerge from 
  https://svn.kde.org/home/kde/branches/kdepim/enterprise/kdepim
  
  ........
    r1030016 | winterz | 2009-10-01 07:56:20 -0400 (Thu, 01 Oct 2009) | 4 lines
    
    crash guard in getHolidays if the specified date is invalid
    possible fix for kolab/issue3889
    MERGE: e4,trunk,4.3
  ........
................


------------------------------------------------------------------------
r1035114 | winterz | 2009-10-14 10:44:06 +0000 (Wed, 14 Oct 2009) | 2 lines

add_definitions -DQT_GUI_LIB for building with Qt4.6

------------------------------------------------------------------------
r1035634 | dfaure | 2009-10-15 14:52:47 +0000 (Thu, 15 Oct 2009) | 4 lines

Backport r1035633:
Fix migration of old-style smtp passwords (into the wallet) happening over and
over again for sabine: the key password-kmail was read, but not deleted after migration.

------------------------------------------------------------------------
r1036874 | winterz | 2009-10-18 01:00:01 +0000 (Sun, 18 Oct 2009) | 10 lines

Backport r1036873 by winterz from trunk to the 4.3 branch:

Remove the mDeletedIncidences list. As it was, the mDeletedIncidences.clearAll() in the
close() method was crashy; it should have just been mDeletedIncidences.clear().

Upon further inspection, I realized mDeletedIncidences wasn't even used anymore.
MERGE: 4.3



------------------------------------------------------------------------
r1042472 | mueller | 2009-10-29 21:34:05 +0000 (Thu, 29 Oct 2009) | 2 lines

bump version

------------------------------------------------------------------------
