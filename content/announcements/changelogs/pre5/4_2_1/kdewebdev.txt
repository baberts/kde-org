------------------------------------------------------------------------
r914652 | scripty | 2009-01-21 13:43:02 +0000 (Wed, 21 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r915136 | scripty | 2009-01-22 13:53:06 +0000 (Thu, 22 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r916513 | scripty | 2009-01-25 12:53:30 +0000 (Sun, 25 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r918226 | scripty | 2009-01-29 17:19:54 +0000 (Thu, 29 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r919511 | scripty | 2009-02-01 08:44:45 +0000 (Sun, 01 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r928057 | amantia | 2009-02-18 20:31:43 +0000 (Wed, 18 Feb 2009) | 5 lines

Make it more robust, don't crash when a connection has invalid
sender/receiver. Also don't crash (in debug mode) if the
AboutDialog::addAuthor misses the optional arguments. (automatic
backport of r928017)

------------------------------------------------------------------------
r930455 | mueller | 2009-02-23 14:42:42 +0000 (Mon, 23 Feb 2009) | 2 lines

remove quanta from 4.2 branch

------------------------------------------------------------------------
