------------------------------------------------------------------------
r916511 | scripty | 2009-01-25 12:52:36 +0000 (Sun, 25 Jan 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r916781 | osterfeld | 2009-01-26 00:01:06 +0000 (Mon, 26 Jan 2009) | 7 lines

backport:

SVN commit 916780 by osterfeld:
also include link in hash calculation, otherwise items where only the
links differ are seen as one item.
CCBUG: 141776 

------------------------------------------------------------------------
r917250 | ogoffart | 2009-01-27 12:03:57 +0000 (Tue, 27 Jan 2009) | 3 lines

Fix crash when viewing an email


------------------------------------------------------------------------
r917474 | smartins | 2009-01-27 21:51:09 +0000 (Tue, 27 Jan 2009) | 2 lines

When an incidence is deleted don't forget to notify registered observers.

------------------------------------------------------------------------
r917783 | cgiboudeaux | 2009-01-28 14:51:57 +0000 (Wed, 28 Jan 2009) | 1 line

Backport r917740: kdepimlibs now requires kdelibs 4.2.0
------------------------------------------------------------------------
r917890 | smartins | 2009-01-28 18:27:22 +0000 (Wed, 28 Jan 2009) | 2 lines

Fix Event::dtEnd() so it also returns an inclusive date in case !hasEndDate() && hasDuration().

------------------------------------------------------------------------
r918215 | tmcguire | 2009-01-29 16:54:46 +0000 (Thu, 29 Jan 2009) | 4 lines

Backport r917899 by tmcguire from trunk to the 4.2 branch:

Fix unit test. kabc-addresstest still fails, but that is broken since ages.

------------------------------------------------------------------------
r920496 | smartins | 2009-02-03 01:37:00 +0000 (Tue, 03 Feb 2009) | 2 lines

Fix a Qt4 porting bug. No need to discard the last byte as it's not a NUL anymore.

------------------------------------------------------------------------
r921344 | smartins | 2009-02-04 20:35:40 +0000 (Wed, 04 Feb 2009) | 5 lines

My patch for bug 106672 (Events with duration P1D show up as two days) was missing a return statement.
So while it worked for P1D events, PXD events would show up as single day events.

Thanks to David Jarvie for catching this.

------------------------------------------------------------------------
r921567 | coolo | 2009-02-05 08:40:33 +0000 (Thu, 05 Feb 2009) | 2 lines

translate the strings (reported on german translators list)

------------------------------------------------------------------------
r921657 | coolo | 2009-02-05 13:08:38 +0000 (Thu, 05 Feb 2009) | 2 lines

load the right messages

------------------------------------------------------------------------
r925739 | smartins | 2009-02-14 01:45:23 +0000 (Sat, 14 Feb 2009) | 4 lines

Emit calendarLoaded() in CalendarResources:load() as specified in KCal:Calendar's API docs.

Fixes kontact needing a refresh to show the incidences at startup.

------------------------------------------------------------------------
r926581 | winterz | 2009-02-15 18:51:31 +0000 (Sun, 15 Feb 2009) | 6 lines

backport SVN commit 913835 by otrichet:

Force a disconnection if a timeout is detected, otherwise a latter command would fail because the connection state is unknown.
Fix https://bugzilla.novell.com/show_bug.cgi?id=440029


------------------------------------------------------------------------
r926658 | krake | 2009-02-15 22:17:55 +0000 (Sun, 15 Feb 2009) | 10 lines

Backport of Revision 926657

Fixing implementation of doSave(bool, Incidence*)

Current implementation results in an endless recursion (method calling itself).
This is just never triggered because the subclasses, especially ResourceCached override it.

Call doSave(bool) instead. This also matches the behavior it is said to have according to the API docs


------------------------------------------------------------------------
r926671 | smartins | 2009-02-15 22:56:48 +0000 (Sun, 15 Feb 2009) | 4 lines

Make CalendarResources::endChange() save when incidences are deleted.

Discussion at: http://reviewboard.kde.org/r/96/

------------------------------------------------------------------------
r927694 | scripty | 2009-02-18 06:25:09 +0000 (Wed, 18 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r928173 | smartins | 2009-02-19 01:55:25 +0000 (Thu, 19 Feb 2009) | 2 lines

Don't pass shortfmt to KLocale::formatTime(bool includeSecs), pass !shorfmt

------------------------------------------------------------------------
r928287 | pokrzywka | 2009-02-19 11:25:09 +0000 (Thu, 19 Feb 2009) | 1 line

backported #921930 from trunk : the test program doesn't compile without it on windows (bc the include path doesn't include libical/)
------------------------------------------------------------------------
r929204 | mlaurent | 2009-02-20 21:43:35 +0000 (Fri, 20 Feb 2009) | 2 lines

Backport: fix layout (qprogressbar was not the good place)

------------------------------------------------------------------------
r929529 | vkrause | 2009-02-21 14:52:36 +0000 (Sat, 21 Feb 2009) | 6 lines

Backport SVN commit 929523 by vkrause:

When trying to delete a non-existing item we could end up with a
still open transaction, causing all following changes in the
same session to be non-persistent.

------------------------------------------------------------------------
r929788 | scripty | 2009-02-22 07:11:26 +0000 (Sun, 22 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r929982 | otrichet | 2009-02-22 14:06:15 +0000 (Sun, 22 Feb 2009) | 5 lines

backport SVN commit 913835

Remove assertion on non-textual characters, deal with it
Fix a crash in Knode

------------------------------------------------------------------------
r930071 | vkrause | 2009-02-22 16:46:57 +0000 (Sun, 22 Feb 2009) | 5 lines

Backport SVN commit 930066 by vkrause:

Cancel the various kinds of tasks correctly, especially the item
retrieval as this could cause deadlocks otherwise.

------------------------------------------------------------------------
r930769 | scripty | 2009-02-24 08:18:50 +0000 (Tue, 24 Feb 2009) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r930782 | osterfeld | 2009-02-24 08:44:56 +0000 (Tue, 24 Feb 2009) | 9 lines

backport 924447 and subsequent build fixes:

fix a memleak caused by a shared_ptr cycle: Resource holds a Model copy, which has a shared_ptr<ModelPrivate>, which in turn has a shared_ptr to the 
resources.
That way RDF models never got destructed. Break the cycle by holding weak_ptr's to ModelPrivate in Resource and Statement. That would break cases 
where only a ResourcePtr is kept and no Model copy, thus keep a Model copy in ResourceWrapper so that the model is not destroyed until the last 
Resource/ResourceWrapper refering to that model goes out of scope.


------------------------------------------------------------------------
r930954 | osterfeld | 2009-02-24 14:49:36 +0000 (Tue, 24 Feb 2009) | 2 lines

svn add model_p.h...

------------------------------------------------------------------------
r931305 | tmcguire | 2009-02-25 07:51:45 +0000 (Wed, 25 Feb 2009) | 6 lines

Backport r929690 by tmcguire from trunk to the 4.2 branch:

Fix the problem that my todos would loose hieracy information under certain 
circumstances, and add a test for that.


------------------------------------------------------------------------
r931565 | cgiboudeaux | 2009-02-25 13:46:27 +0000 (Wed, 25 Feb 2009) | 4 lines

Backport r931560 :
RUN_RESULT EQ 1 -> RUN_RESULT EQUAL 1


------------------------------------------------------------------------
