------------------------------------------------------------------------
r1109770 | scripty | 2010-04-01 15:10:11 +1300 (Thu, 01 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1114961 | scripty | 2010-04-15 13:42:23 +1200 (Thu, 15 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1115346 | scripty | 2010-04-16 13:51:28 +1200 (Fri, 16 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1115601 | scripty | 2010-04-17 13:42:57 +1200 (Sat, 17 Apr 2010) | 1 line

SVN_SILENT made messages (.desktop file)
------------------------------------------------------------------------
r1116624 | aseigo | 2010-04-20 09:33:10 +1200 (Tue, 20 Apr 2010) | 2 lines

backport fix for bug 207982: turn the chain of if's into else if's so that if the name var gets changed out from under us in that method, it doesn't end up executing unwanted branches; patch by Lasse Liehu

------------------------------------------------------------------------
r1118482 | aacid | 2010-04-25 08:27:29 +1200 (Sun, 25 Apr 2010) | 22 lines

backport fixes
r1118477 | aacid | 2010-04-24 21:15:22 +0100 (ds, 24 abr 2010) | 2 lines

if KCompactDisc::cdromDeviceUrl gets passed an url instead of a device name just return the url instead of giving the url of the default device

r1118453 | aacid | 2010-04-24 20:44:19 +0100 (ds, 24 abr 2010) | 2 lines

add some const to make the code easier to read

r1118450 | aacid | 2010-04-24 20:38:16 +0100 (ds, 24 abr 2010) | 2 lines

add I18N_NOOP here

r1118448 | aacid | 2010-04-24 20:34:31 +0100 (ds, 24 abr 2010) | 2 lines

do not use *it here since it's obviously pointing to a wrong position

r1118445 | aacid | 2010-04-24 20:24:50 +0100 (ds, 24 abr 2010) | 2 lines

constify the variables that never change


------------------------------------------------------------------------
r1120150 | trueg | 2010-04-29 01:35:38 +1200 (Thu, 29 Apr 2010) | 1 line

Backport: Try to stat any local protocol for a local path instead of just media. That way nepomuk search URLs are properly opened.
------------------------------------------------------------------------
