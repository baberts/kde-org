---
title: KDE Ships KDE Applications 19.04.1
major_version: "19.04"
version: "19.04.1"
release: applications-19.04.1
description: KDE Ships Applications 19.04.1.
date: 2019-05-09
changelog: fulllog_applications-19.04.1
layout: application
---

May 09, 2019.

{{% i18n_var "Today KDE released the first stability update for <a href='%[1]s'>KDE Applications %[2]s</a>. This release contains only bugfixes and translation updates, providing a safe and pleasant update for everyone." "../announce-applications-19.04.0" "19.04" %}}

About twenty recorded bugfixes include improvements to Kontact, Ark, Cantor, Dolphin, Kdenlive, Spectacle, Umbrello, among others.

Improvements include:

- Tagging files on the desktop no longer truncates the tag name
- A crash in KMail's text sharing plugin has been fixed
- Several regressions in the video editor Kdenlive have been corrected
