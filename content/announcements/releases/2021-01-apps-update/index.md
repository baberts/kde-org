---
title: KDE's January 2021 Apps Update
publishDate: "2021-01-07T12:00:00+00:00" # todo change me
layout: page
type: announcement
---

This month, we welcome a new chat app for Matrix, while KIO Fuse brings better integration with files on a local network.

## Neochat 1.0

{{< img class="text-center img-fluid" src="neochat.png" caption="Neochat" style="width: 600px">}}

Back in 2019, KDE added Matrix to its instant messaging services. Matrix brings KDE's chat channels up to date with persistent connections you can use from all your devices, lets you send pictures and includes plugins for things like video chat.

Now KDE has its own Matrix app too: NeoChat. NeoChat is written using our Kirigami user interface framework which gives the app a modern look and feel and allows it to adapt well to all your devices, be it your desktop, laptop, tablet or phone.

[Read the full announcement](https://carlschwan.eu/2020/12/23/announcing-neochat-1.0-the-kde-matrix-client/) for more information.

You can get NeoChat from [Flathub](https://flathub.org/apps/details/org.kde.neochat) or from your distribution's repos.

Soon NeoChat will be available for Windows and Android too.

## Addons

{{< img class="text-center img-fluid" src="kio-fuse.png" caption="KIO Fuse" style="width: 600px">}}

The other big news this week is KIO Fuse, an addon for KDE apps that improves the integration of remote files with your desktop.

While apps using KDE frameworks, like Kate or Dolphin, can talk directly to network protocols such as Windows' SMB or SSH, other apps like LibreOffice cannot. Instead, KDE used to make local copies of remote files that were then copied back and forth between the servers and the desktop. This proved to be unreliable and could lead to the data loss. Now, using KIO Fuse, all apps will be able to talk to remote files as if they were local, removing the problems of making a temporary copies.

Distributions such as openSUSE tumbleweed and KDE neon have already started shipping with KIO Fuse pre-installed.

## Bugfixes

[KDevelop 5.6.1](https://www.kdevelop.org/news/kdevelop-561-released) was released this month and adds support for Python 3.9 and Gdb 10. Several crashes on exit while debugging were fixed and the behavior of the "Stop" and "Stop all" toolbar buttons were made less confusing.

## Incoming

New apps that have moved into KDE include:

[MyGnuHealth](https://mail.kde.org/pipermail/kde-community/2020q4/006664.html), a component of [GNU Health](https://www.gnuhealth.org), the Libre digital health ecosystem. MyGNUHealth will be able to track your heart rate, blood pressure and other vital signs, nutritional and lifestyle factors that have help you keep fit. It became a KDE project this month.

[KGeoTag](https://invent.kde.org/graphics/kgeotag) is a Free/Libre Open Source photo geotagging program and it is now in beta.

[Kongress](https://invent.kde.org/utilities/kongress), an app which provides practical information about conferences such as the schedule and locations of talks at the venue, passed the kdereview stage this month.

## Releases 20.12.1

Some of our projects release on their own timescale and some get released en-masse. The 20.12.1 bundle of projects was released today with dozens of bugfixes and will be available through app stores and distros soon. See the [20.12.1 releases page](https://www.kde.org/info/releases-20.12.1) for details.

Some of the fixes in today's bugfix releases include:

 * Several causes for crashes when opening Dolphin tabs were fixed
 * Markdown files can again be opened with the Okular document viewer
 * Gwenview now correctly saves the JPEG quality setting
 * KDE's interactive geometry tool Kig no longer crashes when exporting constructions
 * A regression with bold text colors in Konsole was reverted

[20.12 release notes](https://community.kde.org/Releases/20.12_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull; [20.12.1 source info page](https://kde.org/info/releases-20.12.1) &bull; [20.12.1 full changelog](https://kde.org/announcements/fulllog_releases-20.12.1/)

## Contributing

If you would like to help, [get involved](https://community.kde.org/Get_Involved)! We are a friendly and accepting community and we need developers, writers, translators, artists, documenters, testers and evangelists. You can also visit our [welcome chat channel](https://webchat.kde.org/#/room/#kde-welcome:kde.org) and talk live to active KDE contributors.

Another way to help KDE is by [donating to KDE](https://kde.org/community/donations/) and helping the Community keep things running.
