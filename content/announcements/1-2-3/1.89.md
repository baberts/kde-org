---
aliases:
- ../announce-1.89
date: '1999-12-15'
title: KDE 1.89 Release Announcement
---

## Gearing Towards 2nd Generation Desktop Environment

The <a href="/">K Desktop Environment (KDE) Team</a>
is happy to announce KRASH,
an unstable developers release of the upcoming KDE 2.0 Desktop
Environment.

KRASH is intended to provide a stable API for KDE developers who are
outside the main KDE project to begin porting their applications to.
While many of the core KDE 2.0 applications are incomplete or bugg
at this point due to a focus on base library functionality, this snapshot
does provide an API consistent with what will be released in the final
version. Where changes occur, they will be fully documented.

This is not a snapshot intended to be usable by end users. We strongly
recommend users remain with the latest stable version, KDE 1.1.2,
until KDE 2.0 will be released. Even people developing for the new
KDE 2.0 might wish to remain with KDE 1.1.2 as their primary desktop
system.

KDE 2.0 is scheduled to be released in the spring of 2000. This date is
subject to change without further notice.

### WHERE TO DOWNLOAD

KRASH can be downloaded from:

<a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/tar/generic">
ftp://ftp.kde.org/pub/kde/unstable/distribution/tar/generic</a>

Or one of its mirror sites.

KRASH requires a recent snapshot of the Qt 2.1 toolkit.
<a href="ftp://ftp.kde.org/pub/kde/unstable/distribution/tar/generic/qt-2.1.0-snapshot-19991215.tar.gz">
The 15 Dec 1999
version of such a snapshot</a> is provided at the above location for your
convenience. Please note that this version of Qt has not been released
yet by Trolltech and that this version is not officially supported by either
KDE or Trolltech. KRASH does NOT work with Qt 1.x or Qt 2.0.

### INCLUDED IN THIS RELEASE

<table>
<tr><td width=100>kdesupport</td><td>- General support libraries</td></tr>
<tr><td>kdelibs</td><td>- KDE libraries</td></tr>
<tr><td>kdebase</td><td>- KDE desktop environment</td></tr>
<tr><td>koffice</td><td>- KDE office suite</td></tr>
</table>

### GOAL OF THE KRASH (1.89) RELEASE

- Stabilize development.
- Offer third party developers a convenient way to become familiar with
  Qt2.x / KDE 2.x

### REMARKS

KRASH is alpha quality software. It is not recommended to use
KRASH for daily work. Although the API of KRASH should be relatively
stable, CHANGES will be made to it before the final KDE 2.0 release.
It is not recommended to publically release applications based on this
snapshot.

Distributors should NOT SHIP this release without providing a stable
and complete version of KDE as well.

### PLANNED CHANGES

In the following areas major changes are foreseen between now and KDE 2.0:

- Interface and launch method of the IO-Slaves (kio / kioslaves)
- File selection dialog (kfile rewrite)
- Parts embedding (kparts redesign)
- Advanced features of HTML widget (khtml)
- Embedding within kicker (kapplet)
- Sound system

### ABOUT KDE

KDE is a collaborative Open Source project by hundreds of developers
worldwide to create a sophisticated, customizable and stable desktop
environment employing a network-transparent, intuitive user interface.
KDE includes applications for email, web browsing, system
administration, dial-up networking, scheduling, Palm Pilot(r)
synchronization, and many other tasks. KDE is working proof of the
power of the open source software development model.

The elegance and usefulness of the KDE environment has been recognized
through multiple awards, including Linux World magazine's "Editor's
Choice" award in the Desktop Environment category, and Ziff-Davis'
"Innovation of the year 1998/1999" award in the Software category.

For more information about KDE, please visit
<a href="/whatiskde">http://www.kde.org/whatiskde/</a>.
