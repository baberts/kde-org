---
title: Release of KDE Frameworks 5.8.0
date: "2015-03-13"
description: KDE Ships Frameworks 5.8.0.
---

{{<figure src="/announcements/frameworks5TP/KDE_QT.jpg" class="text-center" caption="Collaboration between Qt and KDE" >}}

March 13, 2015. KDE today announces the release of KDE Frameworks 5.8.0.

KDE Frameworks are 60 addon libraries to Qt which provide a wide variety of commonly needed functionality in mature, peer reviewed and well tested libraries with friendly licensing terms. For an introduction see <a href='http://kde.org/announcements/kde-frameworks-5.0.php'>the Frameworks 5.0 release announcement</a>.

This release is part of a series of planned monthly releases making improvements available to developers in a quick and predictable manner.

## New in this Version

New frameworks:

- KPeople, provides access to all contacts and the people who hold them
- KXmlRpcClient, interaction with XMLRPC services

### General

- A number of build fixes for compiling with the upcoming Qt 5.5

### KActivities

- Resources scoring service is now finalized

### KArchive

- Stop failing on ZIP files with redundant data descriptors

### KCMUtils

- Restore KCModule::setAuthAction

### KCoreAddons

- KPluginMetadata: add support for Hidden key

### KDeclarative

- Prefer exposing lists to QML with QJsonArray
- Handle non default devicePixelRatios in images
- Expose hasUrls in DeclarativeMimeData
- Allow users to configure how many horizontal lines are drawn

### KDocTools

- Fix the build on MacOSX when using Homebrew
- Better styling of media objects (images, ...) in documentation
- Encode invalid chars in paths used in XML DTDs, avoiding errors

### KGlobalAccel

- Activation timestamp set as dynamic property on triggered QAction.

### KIconThemes

- Fix QIcon::fromTheme(xxx, someFallback) would not return the fallback

### KImageFormats

- Make PSD image reader endianess-agnostic.

### KIO

- Deprecate UDSEntry::listFields and add the UDSEntry::fields method which returns a QVector without costly conversion.
- Sync bookmarkmanager only if change was by this process (bug 343735)
- Fix startup of kssld5 dbus service
- Implement quota-used-bytes and quota-available-bytes from RFC 4331 to enable free space information in http ioslave.

### KNotifications

- Delay the audio init until actually needed
- Fix notification config not applying instantly
- Fix audio notifications stopping after first file played

### KNotifyConfig

- Add optional dependency on QtSpeech to reenable speaking notifications.

### KService

- KPluginInfo: support stringlists as properties

### KTextEditor

- Add word count statistics in statusbar
- vimode: fix crash when removing last line in Visual Line mode

### KWidgetsAddons

- Make KRatingWidget cope with devicePixelRatio

### KWindowSystem

- KSelectionWatcher and KSelectionOwner can be used without depending on QX11Info.
- KXMessages can be used without depending on QX11Info

### NetworkManagerQt

- Add new properties and methods from NetworkManager 1.0.0

#### Plasma framework

- Fix plasmapkg2 for translated systems
- Improve tooltip layout
- Make it possible to let plasmoids to load scripts outside the plasma package
  ...

### Buildsystem changes (extra-cmake-modules)

- Extend ecm_generate_headers macro to also support CamelCase.h headers

You can discuss and share ideas on this release in the comments section of <a href='https://dot.kde.org/2014/07/07/kde-frameworks-5-makes-kde-software-more-accessible-all-qt-developers'>the dot article</a>.

