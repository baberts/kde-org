---
title: KDE Desktop Is Show Favorite at LinuxWorld Expo
date: "2000-08-22"
description: August 22, 2000 (The INTERNET). KDE, the most advanced and user-friendly desktop for GNU/Linux - UNIX operating systems, was honored as Show Favorite in the Desktop category at the LinuxWorld Expo, held in San Jose, California last week.
---

FOR IMMEDIATE RELEASE

GNU/Linux Users Speak: KDE Is the Best Desktop for GNU/Linux<SUP>&reg;</SUP>

August 22, 2000 (The INTERNET). <a href="/">KDE</a>,
the most advanced and user-friendly desktop for GNU/Linux - UNIX<SUP>&reg;</SUP>
operating systems, was honored as
<a href="http://www.kde.org/img/awards/LWE-award_08_2000.png">Show Favorite</a>
in the Desktop category at the LinuxWorld Expo, held in San Jose,
California last week.

KDE developers and supporters were delighted KDE was selected the best
desktop by GNU/Linux users.
"KDE development has always been dedicated to meeting the needs of
users, and this award from GNU/Linux users proves this dedication is
appreciated," said Dirk Hohndel, Vice President of XFree86, Inc. and
CTO of SuSE Linux AG. "We believe that KDE and KOffice form an
unparalleled next-generation GNU/Linux desktop and will pave the way to the
widespread acceptance of GNU/Linux for desktop users."

"And the best is yet to come", added Matthias Ettrich, founder of
the KDE project. "Tomorrow we will
<a href="http://www.kde.org/announcements/announce-1.93.html">announce</a> the
availability of the fourth beta release for KDE 2. KDE 2, scheduled for
final release in mid-fourth quarter, is a
next-generation, component-based desktop which will set new standards of
quality and ease of use for a UNIX desktop. It will also provide an
advanced, developer-friendly platform for both free and proprietary software."

This award reaffirms that KDE is the desktop of choice for GNU/Linux users.
In two surveys
(<a href="http://www.borland.com/linux/jul99survey/linuxprimary.html">1</a>,
<a href="http://www.borland.com/linux/jul99survey/windowsprimary.html">2</a>)
conducted</a> by <a href="http://www.borland.com/">Borland</a> last year,
over half of the respondents indicated they run KDE as their desktop
environment. Moreover, KDE has earned many other awards, including:

<ul>
<li>"<a href="http://www.zdnet.de/news/artikel/1999/03/19011-wf.htm">Innovation
of the Year 1998/99</a>" in the category "Software" at
<a href="http://www2.cebit.de/">CeBIT</a>, the world's largest computer trade
fair;</LI>
<li>"Readers' Choice awards for 1999"
for best "Window Manager"
from the <a href="http://www.linuxjournal.com/">Linux Journal</a>;</LI>
<li>"<a href="http://www.linuxworld.com/linuxworld/lw-1999-08/lw-08-penguin_1-2.html">Editor's
Choice Award</a>" in the "Desktop Environment" category
from <a href="http://www.linuxworld.com/">LinuxWorld</a>;</LI>
<li>"<a href="http://www.linuxgazette.com/issue35/richardson.html">Editor's
Choice Awards</a>" for "Most Promising Software Newcomers"
from the <a href="http://www.linuxgazette.com/">Linux Gazette</a>; and</LI>
<li>"Best Of the Net" award for two consecutive years from
<a href="http://linux.miningco.com/">The Mining Company</a>.</LI>
</ul>

#### About KDE

KDE's major contributions to GNU/Linux - UNIX are ease of installation,
configuration, use and development. KDE provides users of all nationalities
with an attractive, functional and intuitive desktop based on
reusable components and network transparency. It also provides a framework for
applications that provide a consistent, yet highly customizable, look-and-feel.

For the developer, KDE also offers the best Open Source development platform
featuring an assortment of superb Open Source, free development tools such as
<a href="http://www.kdevelop.org/">KDevelop</a>,
<a href="http://www.thekompany.com/projects/kdestudio/index.php3?dhtml_ok=0">KDE Studio</a>,
and <a href="http://www.trolltech.com/">Trolltech's</a>
<a href="http://www.trolltech.com/products/qt/designer/sshots.html">Qt
Designer</a>, and commercial ones
such as <a href="http://www.borland.com/">Borland's</a> Kylix, the
Linux version of <a href="http://www.borland.com/delphi/">Delphi</a>.

KDE is an independent, collaborative project by hundreds of developers
worldwide. Currently development is focused on KDE 2, which will for
the first time offer a fully object-oriented, component-based desktop and
office suite. KDE 2 promises to make the GNU/Linux desktop as
easy to use as the popular commercial desktops while staying true
to open standards and empowering developers and users with quality
Open Source software.

For more information about KDE, please visit KDE's <a href="http://www.kde.org/whatiskde/">web site</a>.

<hr /><table border=0 cellpadding=8 cellspacing=0 align="center">
<tr>
  <th colspan=2 align="left">
    Press Contacts:
  </th>
</tr>
<tr Valign="top">
  <td >
    United&nbsp;States:
  </td>
  <td >
    Kurt Granroth <br>
    
  [granroth@kde.org](mailto:granroth@kde.org)
    <br>
    (1) 480 732 1752<br>&nbsp;<br>
    Andreas Pour<br>
    [pour@kde.org](pour@kde.org)<br>
    (1) 718-456-1165
  </td>
</tr>
<tr Valign="top">
  <td >
    Europe (English and German):
  </td>
  <td>
    Martin Konold<br>
    
  [konold@kde.org](konold@kde.org)
    <br>
    (49) 179 2252249
  </td>
</tr>
</table>
