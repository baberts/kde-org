---
aliases:
- ../../plasma-5.16.1
changelog: 16.0-16.1
date: 2019-06-18
description: KDE Ships Plasma 5.16.1.
layout: plasma
release: plasma-5.16.1
title: KDE Plasma 5.16.1, Bugfix Release for June
version: 5.16.1
---

{{% youtube id="T-29hJUxoFQ" %}}

{{<figure src="/announcements/plasma/5/16.0/plasma_5.16.png" alt="Plasma 5.16" class="text-center" width="600px" caption="KDE Plasma 5.16">}}

Tuesday, 18 June 2019.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.16.1" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in June with many feature refinements and new modules to complete the desktop experience." "5.16" >}}

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- PanelView: no more transparent line between panels and maximized windows with some 3rd-party Plasma themes. <a href="https://commits.kde.org/plasma-workspace/f65a0eee09dabc66d9d7acf6ddda6bcb03888794">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D21803">D21803</a>
- Breeze theme: Re-read color palettes when application color changes. <a href="https://commits.kde.org/breeze/9d6c7c7f3439941a6870d6537645297683501bb0">Commit.</a> Fixes bug <a href="https://bugs.kde.org/408416">#408416</a>. See bug <a href="https://bugs.kde.org/382505">#382505</a>. See bug <a href="https://bugs.kde.org/355295">#355295</a>. Phabricator Code review <a href="https://phabricator.kde.org/D21646">D21646</a>
- Discover: Flatpak, Indicate that updates are being fetched. <a href="https://commits.kde.org/discover/67b313bdd6472e79e3d500f8b32d0451c236ce84">Commit.</a> Fixes bug <a href="https://bugs.kde.org/408608">#408608</a>
- Powerdevil runner: Make Sleep/Suspend command work again. <a href="https://commits.kde.org/plasma-workspace/6a5a38f1281f630edc8fda18523fe9dceef22377">Commit.</a> Fixes bug <a href="https://bugs.kde.org/408735">#408735</a>