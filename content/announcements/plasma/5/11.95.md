---
aliases:
- ../../plasma-5.11.95
date: 2018-01-15
description: KDE Ships 5.11.95
layout: plasma
release: plasma-5.11.95
title: Plasma 5.12 LTS Beta Makes Your Desktop Smoother and Speedier
version: 5.11.95
---

{{<figure src="/announcements/plasma/5/12.0/plasma-5.12.png" alt="KDE Plasma 5.12 LTS Beta " class="text-center" width="600px" caption="KDE Plasma 5.12 LTS Beta">}}

Monday, 15 Jan 2018.

Plasma 5.12 LTS Beta is the second long term support release from the Plasma 5 team. We have been working hard focusing on speed and stability for this release. Boot time to desktop has been improved by reviewing the code for anything which blocks execution. The team has been triaging and fixing bugs in every aspect of the codebase, tidying up artwork, removing corner cases and ensuring cross desktop integration. For the first time we offer our Wayland integration on long term support so you can be sure we will continue to fix bugs over the coming releases.

## New in Plasma 5.12 LTS

### Smoother and Speedier

We have been concentrating on speed and memory improvements with this long term support release. When Plasma is running it now uses less CPU and less memory than previous versions. The time it takes to start a Plasma desktop has been reduced dramatically.

### Plasma on Wayland Now Under LTS promise

{{<figure src="/announcements/plasma/5/12.0/kscreen-wayland.png" alt="Display Setup Now Supports Wayland " class="text-center" width="600px" caption="Display Setup Now Supports Wayland">}}

Plasma's support for running as Wayland is now more complete and is now suitable for more wide-ranged testing. It is included as Long Term Support for the first time so we will be fixing bugs in the 5.12 LTS series. New features include:

- Output resolution can be set through KScreen
- Enable/Disable outputs through KScreen
- Screen rotation
- Automatic screen rotation based on orientation sensor
- Automatic touch screen calibration
- XWayland is no longer required to run the Plasma desktop; applications only supporting X still make use of it.
- Wayland windows can be set to fullscreen
- Uses real-time scheduling policy to keep input responsive
- Automatic selection of the Compositor based on the used platform
- Starts implementing window rules
- KWin integrated Night Color removes blue light from your screen at night-time; this is a Wayland-only replacement for the great Redshift app on X

For those who know their Wayland internals the protocols we added are:

- xdg_shell_unstable_v6
- xdg_foreign_unstable_v2
- idle_inhibit_unstable_v1
- server_decoration_palette
- appmenu
- wl_data_device_manager raised to version 3

Important change of policy: 5.12 is the last release which sees feature development in KWin on X11. With 5.13 onwards only new features relevant to Wayland are going to be added.

We have put a lot of work into making Wayland support in Plasma as good as possible but there are still some missing features and issues in certain hardware configurations so we don't yet recommend it for daily use. More information on the <a href='https://community.kde.org/Plasma/Wayland_Showstoppers'>Wayland status wiki page</a>.

### Discover

{{<figure src="/announcements/plasma/5/12.0/discover-app.png" alt="Discover's new app page " class="text-center" width="600px" caption="Discover's new app page">}}

- Improvements in User Interface

- Redesigned app pages to showcase great software
- Leaner headers on non-browsing sections
- More compact browsing views, so you can see more apps at once
- App screenshots are bigger and have keyboard navigation
- Installed app list sorted alphabetically
- More complete UI to configure sources

- Much greater stability
- Improvements in Snap and Flatpak support
- Support for apt:// URLs
- Distributions can enable offline updates
- Better usability on phone form factors: uses Kirigami main action, has a view specific for searching
- Integrate PackageKit global signals into notifications

- Distro upgrade for new releases
- Reboot notification for when Discover installs or updates a package that needs a reboot

### New Features

{{<figure src="/announcements/plasma/5/12.0/weather-applet.png" alt="Weather Applet with Temperature " class="text-center" width="600px" caption="Weather Applet with Temperature">}}

{{<figure src="/announcements/plasma/5/12.0/system-monitor.png" alt="CPU usage in System Activity " class="text-center" width="600px" caption="CPU usage in System Activity">}}

{{<figure src="/announcements/plasma/5/12.0/window-shadows.png" alt="Larger, horizontally-centered window shadows " class="text-center" width="600px" caption="Larger, horizontally-centered window shadows">}}

- Night Color feature to reduce evening blue light expose
- Usability improvement for the global menu: adding a global menu panel or window decoration button enables it without needing an extra configuration step
- Accessibility fixes in KRunner: it can now be completely used with on-screen readers such as Orca
- Icon applet now uses website favicon
- Notification text selectable again including copy link feature
- Slim Kickoff application menu layout
- The weather applet can now optionally show temperature next to the weather status icon on the panel
- System Activity and System Monitor now show per-process graphs for the CPU usage
- Clock widget's text is now sized more appropriately
- Windows shadows are now horizontally centered, and larger by default
- Properties dialog now shows file metadata

## What’s New Since Plasma 5.8 LTS

If you have been using our previous LTS release, Plasma 5.8, there are many new features for you to look forward to:

## Previews in Notifications

{{<figure src="/announcements/plasma/5/12.0/spectacle-notification.png" alt="Preview in Notifications " class="text-center" width="600px" caption="Preview in Notifications">}}

The notification system gained support for interactive previews that allow you to quickly take a screenshot and drag it into a chat window, an email composer, or a web browser form. This way you never have to leave the application you’re currently working with.

## Task Manager Improvement

{{<figure src="/announcements/plasma/5/12.0/mute.png" alt="Audio Icon and Mute Button Context Menu Entry " class="text-center" width="600px" caption="Audio Icon and Mute Button Context Menu Entry">}}

Due to popular demand we implemented switching between windows in Task Manager using Meta + number shortcuts for heavy multi-tasking. Also new in Task Manager is the ability to pin different applications in each of your activities. And should you want to focus on one particular task, applications currently playing audio are marked with an icon similar to how it’s done in modern web browsers. Together with a button to mute the offending application, this can help you stay focused.

## Global Menus

{{<figure src="/announcements/plasma/5/12.0/global-menus-window-bar.png" alt="Global Menu Screenshots, Applet and Window Decoration " class="text-center" width="600px" caption="Global Menu Screenshots, Applet and Window Decoration">}}

Global Menus have also returned. KDE's pioneering feature to separate the menu bar from the application window allows for new user interface paradigm with either a Plasma Widget showing the menu or neatly tucked away in the window title bar. Setting it up has been greatly simplified in Plasma 5.12: as soon as you add the Global Menu widget or title bar button, the required background service gets started automatically.

## Spring Loaded Folders

{{<figure src="/announcements/plasma/5/12.0/spring_loading.gif" alt="Spring Loading in Folder View " class="text-center" width="600px" caption="Spring Loading in Folder View">}}

Folder View is now the default desktop layout. After some years shunning icons on the desktop we have accepted the inevitable and changed to Folder View as the default desktop which brings some icons by default and allows users to put whatever files or folders they want easy access to. For this many improvements have been made to Folder View, including <a href='https://blogs.kde.org/2017/01/31/plasma-510-spring-loading-folder-view-performance-work'>Spring Loading</a> making drag and drop of files powerful and quick, a tighter icon grid, as well as massively improved performance.

## Music Controls in Lock Screen

{{<figure src="/announcements/plasma/5/12.0/lock-music.png" alt="Media Controls on Lock Screen " class="text-center" width="600px" caption="Media Controls on Lock Screen">}}

Media controls have been added to the lock screen, which can be disabled since Plasma 5.12 for added privacy. Moreover, music will automatically pause when the system suspends.

## New System Settings Interface

{{<figure src="/announcements/plasma/5/12.0/system-settings.png" alt="New Look System Settings " class="text-center" width="600px" caption="New Look System Settings">}}

We introduced a new System Settings user interface for easy access to commonly used settings. It is the first step in making this often-used and complex application easier to navigate and more user-friendly. The new design is added as an option, users who prefer the older icon or tree views can move back to their preferred way of navigation.

## Plasma Vaults

{{<figure src="/announcements/plasma/5/12.0/plasma-vault.png" alt="Plasma Vaults " class="text-center" width="600px" caption="Plasma Vaults">}}

KDE has a focus on privacy. Our vision is: A world in which everyone has control over their digital life and enjoys freedom and privacy.

For users who often deal with sensitive, confidential and private information, the new Plasma Vault offers strong encryption features presented in a user-friendly way. It allows to lock and encrypt sets of documents and hide them from prying eyes even when the user is logged in. Plasma Vault extends Plasma's activities feature with secure storage.

## Plasma's Comprehensive Features

Take a look at what Plasma offers, a comprehensive selection of features unparalleled in any desktop software.

### Desktop Widgets

{{<figure src="/announcements/plasma/5/12.0/widgets.png" alt="Desktop Widgets " class="text-center" width="600px" caption="Desktop Widgets">}}

Cover your desktop in useful widgets to keep you up to date with weather, amused with comics or helping with calculations.

### Get Hot New Stuff

{{<figure src="/announcements/plasma/5/12.0/ghns.png" alt="Get Hot New Stuff " class="text-center" width="600px" caption="Get Hot New Stuff">}}

Download wallpapers, window style, widgets, desktop effects and dozens of other resources straight to your desktop. We work with the new <a href="http://store.kde.org/">KDE Store</a> to bring you a wide selection of addons for you to install.

### Desktop Search

{{<figure src="/announcements/plasma/5/12.0/krunner.png" alt="Desktop Search " class="text-center" width="600px" caption="Desktop Search">}}

Plasma will let you easily search your desktop for applications, folders, music, video, files... everything you have.

### Unified Look

{{<figure src="/announcements/plasma/5/12.0/gtk-integration.png" alt="Unified Look " class="text-center" width="600px" caption="Unified Look">}}

Plasma's default Breeze theme has a unified look across all the common programmer toolkits - Qt 4 &amp; 5, GTK 2 &amp; 3, even LibreOffice.

### Phone Integration

{{<figure src="/announcements/plasma/5/12.0/kdeconnect.png" alt="Phone Integration " class="text-center" width="600px" caption="Phone Integration">}}

Using KDE Connect you'll be notified on your desktop of text message, can easily transfer files, have your music silenced during calls and even use your phone as a remote control.

### Infinitely Customisable

{{<figure src="/announcements/plasma/5/12.0/customisable.png" alt="Infinitely Customisable " class="text-center" width="600px" caption="Infinitely Customisable">}}

Plasma is simple by default but you can customise it however you like with new widgets, panels, screens and styles.

<a href="/announcements/changelogs/plasma/5/11.5-11.95">Full Plasma 5.12 changelog</a>
