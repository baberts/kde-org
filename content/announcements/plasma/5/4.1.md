---
aliases:
- ../../plasma-5.4.1
changelog: 4.0-4.1
date: 2015-09-08
description: KDE Ships Plasma 5.4.1
layout: plasma
release: plasma-5.4.1
title: KDE Ships Plasma 5.4.1, bugfix Release for September
---

{{<figure src="/announcements/plasma/5/4.0/plasma-screen-desktop-2-shadow.png" alt="Plasma 5.4 " class="text-center" width="600px" caption="Plasma 5.4">}}

Tuesday, 08 September 2015.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.4.1" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in August with many feature refinements and new modules to complete the desktop experience." "5.4" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fixes for compilation with GCC 5
- Autostart desktop files no longer saved to the wrong location
- On Muon Make sure the install button has a size.