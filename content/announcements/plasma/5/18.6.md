---
aliases:
- ../../plasma-5.18.6
changelog: 18.5-18.6
hidden: true
plasma: true
title: KDE Plasma 5.18.6 LTS, Bugfix Release for September
version: 5.18.6
---

{{< peertube "cda402b5-2bcb-4c0c-b232-0fa5a4dacaf5" >}}

Tuesday, 29 September 2020.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.18.6" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in February with many feature refinements and new modules to complete the desktop experience." "5.18" >}}

This release adds six months' worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

+ Kcm_fonts: Make the font selection dialog select the correct Regular-like style. <a href="https://commits.kde.org/plasma-desktop/e5e5f5ed51aadfac99bfbdf3d2db5be16a12443b">Commit.</a> See bug <a href="https://bugs.kde.org/420287">#420287</a>
+ Fix calendar events not being shown at first. <a href="https://commits.kde.org/plasma-workspace/97648f015cff39e46e39ee7b150515d1d3bce5f7">Commit.</a>
+ Discover: Confirm reboot action with the user. <a href="https://commits.kde.org/discover/f2dce150e6d32810d1deae08378a136fc13e9988">Commit.</a>