---
aliases:
- ../../plasma-5.4.2
changelog: 4.1-4.2
date: 2015-10-06
description: KDE Ships Plasma 5.4.2
layout: plasma
release: plasma-5.4.2
title: KDE Ships Plasma 5.4.2, bugfix Release for October
---

{{<figure src="/announcements/plasma/5/4.0/plasma-screen-desktop-2-shadow.png" alt="Plasma 5.4 " class="text-center" width="600px" caption="Plasma 5.4">}}

Tuesday, 06 October 2015.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.4.2" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in August with many feature refinements and new modules to complete the desktop experience." "5.4" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- <a href="https://kdeonlinux.wordpress.com/2015/09/15/breeze-is-finished/">Many new Breeze icons</a>.
- Support absolute libexec path configuration, fixes binaries invoked by KWin work again on e.g. Fedora. <a href="http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=85b35157943ab4e7ea874639a4c714a10feccc00">Commit.</a> Fixes bug <a href="https://bugs.kde.org/353154">#353154</a>. Code review <a href="https://git.reviewboard.kde.org/r/125466">#125466</a>
- Set tooltip icon in notifications applet. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=3f8fbd3d4a6b9aafa6bbccdd4282d2538018a7c6">Commit.</a> Code review <a href="https://git.reviewboard.kde.org/r/125193">#125193</a>