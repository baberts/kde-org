---
aliases:
- ../../plasma-5.6.5
changelog: 6.4-6.5
date: 2016-06-14
description: KDE Ships Plasma 5.6.5.
layout: plasma
release: plasma-5.6.5
title: KDE Plasma 5.6.5, bugfix Release for June
---

{{%youtube id="v0TzoXhAbxg"%}}

{{<figure src="/announcements/plasma/5/6.0/plasma-5.6.png" alt="KDE Plasma 5.6 " class="text-center" width="600px" caption="KDE Plasma 5.6">}}

Tuesday, 14 June 2016.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.6.5" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in March with many feature refinements and new modules to complete the desktop experience." "5.6" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Don't let the delegate overflow the view. <a href="http://quickgit.kde.org/?p=discover.git&amp;a=commit&amp;h=1b5ce678c3dc7094d883a0b2b3bbd612207acce8">Commit.</a>
- Battery icon no longer errorneously reports an empty battery if computer has none. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=a19fcfaf90db8ebc6e704917448ccfde7ae0ae59">Commit.</a> See bug <a href="https://bugs.kde.org/362924">#362924</a>
- Create ~/.local/share/mime/packages/ if it doesn't exist. <a href="http://quickgit.kde.org/?p=kde-cli-tools.git&amp;a=commit&amp;h=c2aa2a46d51793d26dc6e93e60b5933cb1193e56">Commit.</a> Fixes bug <a href="https://bugs.kde.org/356237">#356237</a>. Code review <a href="https://git.reviewboard.kde.org/r/128055">#128055</a>