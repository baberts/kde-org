---
aliases:
- ../../plasma-5.20.5
changelog: 20.4-20.5
date: 2021-01-05
description: Today KDE releases a bugfix update to KDE Plasma 5
draft: false
layout: plasma
title: KDE Plasma 5.20.5, bugfix Release for January
---

{{< plasma-5-20-video >}}

Tuesday, 5 January 2021.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.20.5" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in October with many feature refinements and new modules to complete the desktop experience." "5.20" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

+ Plasma NM: Fix password entry jumping to different networks with wifi scanning, by pausing the scan when appropriate. [Commit.](http://commits.kde.org/plasma-nm/21a410ff77049e996df5fdc35215c4b30d893ccc) 
+ Plasma PA: Read text color from proper theme. [Commit.](http://commits.kde.org/plasma-pa/099e925c879ece8f90734ea66c0878bc174c9608) 
+ Plasma Workspace: Move keyboard positioning in the keyboard itself. [Commit.](http://commits.kde.org/plasma-workspace/8c267852a56c74c14a471d266f87ac867b8276d9) Fixes bug [#427934](https://bugs.kde.org/427934)