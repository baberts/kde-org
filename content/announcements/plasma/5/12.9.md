---
aliases:
- ../../plasma-5.12.9
changelog: 12.8-12.9
date: 2019-09-10
description: KDE Ships Plasma 5.12.9.
layout: plasma
release: plasma-5.12.9
title: KDE Plasma 5.12.9, Bugfix Release for March
version: 5.12.9
---

{{%youtube id="xha6DJ_v1E4"%}}

{{<figure src="/announcements/plasma/5/12.0/plasma-5.12.png" alt="KDE Plasma 5.12" class="text-center" width="600px" caption="KDE Plasma 5.12">}}

Tuesday, 10 September 2019.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.12.9" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in February 2018 with many feature refinements and new modules to complete the desktop experience." "5.12" >}}

This release adds six months' worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Make the Trashcan applet use the same shadow settings as desktop icons. <a href="https://commits.kde.org/plasma-desktop/83ef545a35abcaab51e1fd02accf89d26a3ba95c">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D21545">D21545</a>
- [Folder View] Improve label crispness. <a href="https://commits.kde.org/plasma-desktop/05e59e1c77c4a83590b0cd906ecfb698ae5ca3b4">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D20407">D20407</a>
- Media controls on the lock screen are now properly translated. <a href="https://commits.kde.org/plasma-workspace/588aa6be2984038f91167db9ab5bd1f62b9f47e5">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D21947">D21947</a>