---
aliases:
- ../../plasma-5.6.1
changelog: 6.0-6.1
date: 2016-03-29
description: KDE Ships Plasma 5.6.1.
layout: plasma
release: plasma-5.6.1
title: KDE Plasma 5.6.1, Bugfix Release for April
---

{{%youtube id="v0TzoXhAbxg"%}}

{{<figure src="/announcements/plasma/5/6.0/plasma-5.6.png" alt="KDE Plasma 5.6 " class="mt-4 text-center" width="600px" caption="KDE Plasma 5.6">}}

Tuesday, 29 March 2016.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.6.1" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in March with many feature refinements and new modules to complete the desktop experience." "5.6" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fix drawing QtQuickControls ComboBox popups
- Fix untranslatable string in Activities KCM.
- Show ratings in Discover Packagekit backend