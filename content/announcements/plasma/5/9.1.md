---
aliases:
- ../../plasma-5.9.1
changelog: 9.0-9.1
date: 2017-02-14
description: KDE Ships Plasma 5.9.1.
layout: plasma
release: plasma-5.9.1
title: KDE Plasma 5.9.1, Bugfix Release
---

{{%youtube id="lm0sqqVcotA"%}}

{{<figure src="/announcements/plasma/5/9.0/plasma-5.9.png" alt="KDE Plasma 5.9 " class="text-center" width="600px" caption="KDE Plasma 5.9">}}

Tuesday, 7 February 2017.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.9.1" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in January with many feature refinements and new modules to complete the desktop experience." "5.9" >}}

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fix i18n extraction: xgettext doesn't recognize single quotes. <a href="https://commits.kde.org/plasma-desktop/8c174b9c1e0b1b1be141eb9280ca260886f0e2cb">Commit.</a>
- Set wallpaper type in SDDM config. <a href="https://commits.kde.org/sddm-kcm/19e83b28161783d570bde2ced692a8b5f2236693">Commit.</a> Fixes bug <a href="https://bugs.kde.org/370521">#370521</a>