---
aliases:
- ../../plasma-5.16.5
changelog: 16.4-16.5
date: 2019-09-03
description: KDE Ships Plasma 5.16.5.
layout: plasma
release: plasma-5.16.5
title: KDE Plasma 5.16.5, Bugfix Release for September
version: 5.16.5
---

{{% youtube id="T-29hJUxoFQ" %}}

{{<figure src="/announcements/plasma/5/16.0/plasma_5.16.png" alt="Plasma 5.16" class="text-center" width="600px" caption="KDE Plasma 5.16">}}

Tuesday, 3 September 2019.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.16.5" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in June with many feature refinements and new modules to complete the desktop experience." "5.16" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- [weather] [envcan] Add additional current condition icon mappings. <a href="https://commits.kde.org/plasma-workspace/3b61c8c689fe2e656e25f927d956a6d8c558b836">Commit.</a>
- [Notifications] Group only same origin and show it in heading. <a href="https://commits.kde.org/plasma-workspace/ee787241bfff581c851777340c1afdc0e46f7812">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D23583">D23583</a>
- Volume Control: Fix speaker test not showing sinks/buttons. <a href="https://commits.kde.org/plasma-pa/097879580833b745bae0dc663df692d573cf6808">Commit.</a> Phabricator Code review <a href="https://phabricator.kde.org/D23620">D23620</a>