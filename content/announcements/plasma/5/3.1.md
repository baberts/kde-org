---
aliases:
- ../../plasma-5.3.1
changelog: 3.0-3.1
date: 2015-05-26
description: KDE Ships Plasma 5.3.1
layout: plasma
release: plasma-5.3.1
title: KDE Ships Plasma 5.3.1, Bugfix Release for May
---

{{<figure src="/announcements/plasma/5/3.0/plasma-5.3.png" alt="Plasma 5.3 " class="text-center" width="600px" caption="Plasma 5.3">}}

Tuesday, 26 May 2015.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.3.1" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in April with many feature refinements and new modules to complete the desktop experience." "5.3" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- <a href="https://sessellift.wordpress.com/2015/05/14/in-free-software-its-okay-to-be-imperfect-as-long-as-youre-open-and-honest-about-it/">show desktop</a> has now been fixed