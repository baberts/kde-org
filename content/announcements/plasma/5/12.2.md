---
aliases:
- ../../plasma-5.12.2
changelog: 12.1-12.2
date: 2018-02-20
description: KDE Ships 5.12.2
layout: plasma
release: plasma-5.12.2
title: KDE Plasma 5.12.2, Bugfix Release for February
version: 5.12.2
---

{{%youtube id="xha6DJ_v1E4"%}}

{{<figure src="/announcements/plasma/5/12.0/plasma-5.12.png" alt="KDE Plasma 5.12 LTS Beta " class="text-center" width="600px" caption="KDE Plasma 5.12 LTS Beta">}}

Tuesday, 20 February 2018.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.12.2" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in February with many feature refinements and new modules to complete the desktop experience." "5.12" >}}

This release adds a week's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- Fix favicons in firefox bookmarks runner. <a href="https://commits.kde.org/plasma-workspace/93f0f5ee2c275c3328f37675b644c1ce35f75e70">Commit.</a> Fixes bug <a href="https://bugs.kde.org/363136">#363136</a>. Phabricator Code review <a href="https://phabricator.kde.org/D10610">D10610</a>
- System settings: Improve sidebar header visibility. <a href="https://commits.kde.org/systemsettings/6f5b6e41ec4dec6af9693c3a22e5181ee850414b">Commit.</a> Fixes bug <a href="https://bugs.kde.org/384638">#384638</a>. Phabricator Code review <a href="https://phabricator.kde.org/D10620">D10620</a>
- Discover: Don't let the user write the first review for apps they haven't installed. <a href="https://commits.kde.org/discover/01ec02e97016ec17393f09d3cb95e40eb7c21bb2">Commit.</a>