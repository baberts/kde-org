---
aliases:
- ../../plasma-5.3.2
changelog: 3.1-3.2
date: 2015-06-30
description: KDE Ships Plasma 5.3.2
layout: plasma
release: plasma-5.3.2
title: KDE Ships Plasma 5.3.2, Bugfix Release for June
---

{{<figure src="/announcements/plasma/5/3.0/plasma-5.3.png" alt="Plasma 5.3 " class="text-center" width="600px" caption="Plasma 5.3">}}

Tuesday, 30 June 2015.

{{< i18n_var "Today KDE releases a bugfix update to KDE Plasma 5, versioned %[1]s." "5.3.2" >}}

{{< i18n_var "[Plasma %[1]s](/announcements/plasma-%[1]s.0) was released in April with many feature refinements and new modules to complete the desktop experience." "5.3" >}}

This release adds a month's worth of new translations and fixes from KDE's contributors. The bugfixes are typically small but important and include:

- KWin: 'Defaults' should set the title bar double-click action to 'Maximize.'. <a href="http://quickgit.kde.org/?p=kwin.git&amp;a=commit&amp;h=26ee92cd678b1e070a3bdebce100e38f74c921da">Commit.</a>
- Improve Applet Alternatives dialog. <a href="http://quickgit.kde.org/?p=plasma-desktop.git&amp;a=commit&amp;h=0e90ea5fea7947acaf56689df18bbdce14e8a35f">Commit.</a> Fixes bug <a href="https://bugs.kde.org/345786">#345786</a>
- Make shutdown scripts work. <a href="http://quickgit.kde.org/?p=plasma-workspace.git&amp;a=commit&amp;h=96fdec6734087e54c5eee7c073b6328b6d602b8e">Commit.</a>