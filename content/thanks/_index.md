---
title: Thank You
layout: thanks
donations:

  - organisation: DigitalOcean
    description: Sponsors free credits on their cloud platform
    usage: We use a DigitalOcean droplet (virtual private server) to host one of our repository mirrors, and part of the KDE neon infrastructure.
    startdate: 2016-07-04

  - organisation: Datacamp Limited (CDN77.com)
    description: Donation of Content Distribution Network services
    usage: CDN77 is used to globally distribute the common images, stylesheets, javascript and other elements which are used on numerous KDE websites, enhancing performance and improving access to our sites in the process.
    startdate: 2016-05-05

  - organisation: stepping stone GmbH
    description: Donation of a virtual server (mason.kde.org).
    usage: Mason is used to run an Anonymous Git server, providing access to KDE Git repositories over git:// and http://
    startdate: 2013-12-02

  - organisation: Ange Optimization
    description: Sponsors a hosted server (ange.kde.org).
    usage: Ange Optimization has sponsored a server hosted by Hetzner. This server is used as a build node for the KDE CI system.
    startdate: 2013-09-30

  - organisation: Gärtner Datensysteme GmbH & Co. KG
    description: Donation and hosting of a server (sage.kde.org).
    usage: Gärtner has donated a series of virtual machines, which host Scripty and the generation process for docs.kde.org as well as our server monitoring system.
    startdate: 2013-05-06

  - organisation: Dalhousie University & ACORN
    description: Donation and hosting of a server (dalca.kde.org).
    usage: Dalhousie University has donated dalca.kde.org and hosts it, with network connectivity provided by ACORN. This server is used as a build node for the KDE CI system.
    startdate: 2013-05-28

  - organisation: Cloudflare
    description: Donation of CDN services
    usage: Cloudflare protects some of our sites against SQL-injections and XSS-scripting. It also provides a CDN for common images, javascript and stylesheets which are used on multiple sites to optimize performance.
    startdate: 2019-10-01

  - organisation: ClouDNS
    description: Donation of DNS services
    usage: CloudDNS provides anycast nameservers for all our domains. Normal namesevers, DDOS-protected nameservers for our important domains and also GEO-ip-based nameservers for providing the right SCM-mirror in each continent.
    startdate: 2019-10-29

  - organisation: Canonical
    description: Donation and hosting of a server (cano.kde.org)
    usage:  Canonical has donated cano.kde.org and takes care of the hosting. This server hosts a number of websites of varying types, including www.kde.org.
    startdate: 2010-12-04

  - organisation: Interdominios
    description: Donation and hosting of a server (fiesta.kde.org)
    usage: Interdominios has donated fiesta.kde.org and takes care of the hosting. This server is used as a build node for the KDE CI system
    startdate: 2010-07-29

  - organisation: Bytemark
    description: Donation of a virtual server (byte.kde.org)
    usage: Bytemark has provided a small virtual machine which can be used for developers that need shell access to a server. The virtual machine also comes with DNS hosting, which we use for most of our domains.
    startdate: 2010-07-29

  - organisation: GNU/FSF
    description: Donation of a virtual server (bluemchen.kde.org)
    usage: This server is used for a variety of tasks, including crucially being one of two servers supporting the Geolocation DNS service behind anongit.kde.org.
    startdate: 2005-08-01

  - organisation: OSUOSL
    description: Donation of a virtual server (stumptown.kde.org)
    usage: This server is used for a series of small websites, including the Season of KDE management application, Commit Digest and our Limesurvey instance (survey.kde.org)
    startdate: 2005-11-28
---

