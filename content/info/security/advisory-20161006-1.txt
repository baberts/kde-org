KDE Project Security Advisory
=============================

Title:          KMail: HTML injection in plain text viewer
Risk Rating:    Important
CVE:            CVE-2016-7966
Platforms:      All
Versions:       kmail >= 4.4.0
Author:         Andre Heinecke <aheinecke@intevation.de>
Date:           6 October 2016

Overview
========

Through a malicious URL that contained a quote character it
was possible to inject HTML code in KMail's plain text viewer.
Due to the parser used on the URL it was not possible to include
the equal sign (=) or a space into the injected HTML, which greatly
reduces the available HTML functionality. Although it is possible
to include an HTML comment indicator to hide content.

Impact
======

An unauthenticated attacker can send out mails with malicious content
that breaks KMail's plain text HTML escape logic. Due to the limitations
of the provided HTML in itself it might not be serious. But as a way
to break out of KMail's restricted Plain text mode this might open
the way to the exploitation of other vulnerabilities in the HTML viewer
code, which is disabled by default.

Workaround
==========

None.

Solution
========

For KDE Frameworks based releases of KMail apply the following patch to
kcoreaddons:
https://quickgit.kde.org/?p=kcoreaddons.git&a=commitdiff&h=96e562d9138c100498da38e4c5b4091a226dde12

For kdelibs4 based releases apply the following patch:
https://quickgit.kde.org/?p=kdepimlibs.git&a=commitdiff&h=176fee25ca79145ab5c8e2275d248f1a46a8d8cf

Credits
=======

Thanks to Roland Tapken for reporting this issue, Andre Heinecke from
Intevation GmbH for analysing the problems and Laurent Montel for
fixing this issue.


Updated Information (1 November 2016)
=====================================

The above mentioned patches are not enough to fix the vulnerability completely.
This wasn't visible, because the patches for CVE-2016-7967 and CVE-2016-7968 made sure,
that this vulnerability can't harm anymore. 
It only became visible, that this vulnerability isn't closed completely for systems,
that are only affected by this CVE.

For KCoreAddons you need:
 https://quickgit.kde.org/?p=kcoreaddons.git&a=commitdiff&h=96e562d9138c100498da38e4c5b4091a226dde12
for applying this patch you may also need to cherry-pick:
 https://quickgit.kde.org/?p=kcoreaddons.git&a=commitdiff&h=1be7272373d60e4234f1a5584e676b579302b053
(these two are released in KCoreAddons KDE Frameworks 5.27.0)

additionally git commits, to close completely:
 https://quickgit.kde.org/?p=kcoreaddons.git&a=commitdiff&h=5e13d2439dbf540fdc840f0b0ab5b3ebf6642c6a
not needed in the strong sense, but this will give you the additional automatic tests, to test if this CVE is closed:
 https://quickgit.kde.org/?p=kcoreaddons.git&a=commitdiff&h=a06cef31cc4c908bc9b76bd9d103fe9c60e0953f 
(will be part of KCoreAddons KDE Frameworks 5.28.0)

For kdepimlibs 4.14:
 https://quickgit.kde.org/?p=kdepimlibs.git&a=commitdiff&h=176fee25ca79145ab5c8e2275d248f1a46a8d8cf
 https://quickgit.kde.org/?p=kdepimlibs.git&a=commitdiff&h=8bbe1bd3fdc55f609340edc667ff154b3d2aaab1
kdepimlibs is at end of life, so no further release is planned.
