KDE Project Security Advisory
=============================

Title: Insufficient Input Validation By IO Slaves and Webkit Part
Risk Rating: Low
CVE: CVE-2014-8600
Platforms: All
Versions:  kwebkitpart <= 1.3.4, kde-runtime <= 4.14.3, kio-extras <= 5.1.1
Author: Albert Astals Cid <aacid@kde.org>
Date: 13 November 2014

Overview
========

kwebkitpart and the bookmarks:// io slave were not sanitizing input correctly allowing to
some javascript being executed on the context of the referenced hostname. For example going to
   bookmarks://hhdhdhhdhdhdh.google.com/'><script>alert('bookmarks'+document.domain);</script>
in Konqueror makes a Javascript alert popup.

Impact
======

Whilst in most cases, the JavaScript will be executed in an untrusted context, with the bookmarks IO slave,
it will be executed in the context of the referenced hostname. In the example above, this is hhdhdhhdhdhdh.google.com.
It should however be noted that KDE mitigates this risk by attempting to ensure that such URLs cannot be embedded directly
into Internet hosted content.

Solution
========

Update to newer versions of kwebkitpart, kde-runtime, kio-extras when released.
Meanwhile apply the following patches:
  kwebkitpart
    http://quickgit.kde.org/?p=kwebkitpart.git&a=commit&h=641aa7c75631084260ae89aecbdb625e918c6689

  kde-runtime
    http://quickgit.kde.org/?p=kde-runtime.git&a=commit&h=d68703900edc8416fbcd2550cd336cbbb76decb9

  kio-extras
    http://quickgit.kde.org/?p=kio-extras.git&a=commit&h=13155c8eb71d1c946bea21c38ea0f8ca7c7013cd

Credits
=======

Thanks to Tim Brown and Darron Burton of Portcullis Security for reporting
Thanks to David Daure and Martin Sandsmark for the patches
