---
version: "15.03.95"
title: "KDE Applications 15.03.95 Info Page"
announcement: /announcements/announce-applications-15.04-beta3
type: info/application-v1
build_instructions: https://techbase.kde.org/Getting_Started/Build/Historic/KDE4
---
