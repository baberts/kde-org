#!/usr/bin/python
#
# GCompris - l10n-fetch-po-files.py
#
# Copyright (C) 2015 Trijita org <jktjkt@trojita.org>
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, see <http://www.gnu.org/licenses/>.
import os
import sys
import subprocess

# Copied from Trojita and GCompris-net
"""Fetch the .po files from KDE's SVN for KDE.org

Run me from KDE.org top-level directory.
"""


SVN_PATH = "svn://anonsvn.kde.org/home/kde/trunk/l10n-kf5/"
SOURCE_PO_PATHS = ["/messages/websites-kde-org/www_www.po", "/messages/websites-kde-org/promo.po", "/messages/websites-kde-org/release_announcements.po", "/messages/websites-kde-org/menu_footer_shared.po"]
OUTPUT_PO_PATH = "./pos/"
OUTPUT_PO_PATTERN = "%s/%s.po"

if not os.path.exists(OUTPUT_PO_PATH):
    os.mkdir(OUTPUT_PO_PATH)

all_languages = "ar bs ca ca@valencia cs de el es et eu fa fi fr  gl hu it ja ko lt nl pl pt pt_BR ru sk sv tr uk zh_CN zh_TW"
all_languages = [x.strip() for x in all_languages.split(" ") if len(x)]

for lang in all_languages:
    print(lang)
    if not os.path.exists(OUTPUT_PO_PATH + '/' + lang):
        os.mkdir(OUTPUT_PO_PATH + '/' + lang)
    for SOURCE_PO_PATH in SOURCE_PO_PATHS:
        name = os.path.splitext(os.path.basename(SOURCE_PO_PATH))[0]
        try:
            raw_data = subprocess.check_output(['svn', 'cat', SVN_PATH + lang + SOURCE_PO_PATH],
                                              stderr=subprocess.PIPE)
            print("Fetched {}".format(name))
            with open(OUTPUT_PO_PATH + OUTPUT_PO_PATTERN % (lang, name), "wb") as f:
                f.write(raw_data)
        except subprocess.CalledProcessError:
            print("No data for {}".format(name))
